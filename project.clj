(defproject ses-api "0.1.0"
  :description   "Emailer: high level email sender API"
  :url           "https://gitlab.com/krrronos/ses-api"
  :license       {:name "Eclipse Public License"
                  :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies  [; clojure
                  [org.clojure/clojure "1.9.0"]
                  [org.clojure/core.async "0.4.474"]
                  ; logging stuff
                  [ch.qos.logback/logback-classic "1.2.3"]
                  [org.slf4j/slf4j-api "1.7.25"]
                  [org.slf4j/jul-to-slf4j "1.7.25"]
                  [org.slf4j/jcl-over-slf4j "1.7.25"]
                  [org.slf4j/log4j-over-slf4j "1.7.25"]
                  [org.clojure/tools.logging "0.4.0"]
                  ; sentry
                  [com.getsentry.raven/raven-logback "7.7.0"]
                  [com.stuartsierra/component "0.3.2"]
                  ; pedestal
                  [io.pedestal/pedestal.service "0.5.3"]
                  [io.pedestal/pedestal.route   "0.5.3"]
                  [io.pedestal/pedestal.jetty   "0.5.3"]
                  ; utils
                  [prismatic/plumbing "0.5.5"]
                  [slingshot "0.12.2"]
                  ; environment management
                  [environ "1.1.0"]
                  ; rabbitmq
                  [com.novemberain/langohr "5.0.0" :exclusions [commons-io commons-codec]]
                  ; database stuff
                  [org.clojure/java.jdbc "0.7.5"]
                  [honeysql "0.9.2"]
                  [nilenso/honeysql-postgres "0.2.3"]
                  [hikari-cp "2.2.0"]
                  [org.postgresql/postgresql "42.2.2"]
                  [instilled/cljdbc "0.0.1-20160915.233511-1"]
                  ; json
                  [cheshire "5.8.0"]
                  ; bcrypt for password
                  [crypto-password "0.2.0"]
                  ; redis
                  [com.taoensso/carmine "2.18.0" :exclusions [commons-codec]]
                  ; sendgrid
                  [clj-sendgrid "0.1.2"]
                  ; mailgun
                  [nilenso/mailgun "0.2.3"]
                  ; background job
                  [twarc "0.1.10"
                   :exclusions [[org.clojure/clojure]
                                [org.clojure/core.async]
                                [com.stuartsierra/component]
                                [prismatic/plumbing]]]]
  :resource-paths ["config" "resources"]
  :profiles       {:dev     {:source-paths ["dev"]
                             :dependencies [[io.pedestal/pedestal.service-tools "0.5.3"]
                                            [pjstadig/humane-test-output "0.8.3"]
                                            [reloaded.repl "0.2.4"]]
                             :resource-paths ["test/resources"]}
                   :uberjar {:aot :all}}
  :min-lein-version "2.0.0"
  :uberjar-name   "ses-api-0.1.0-standalone.jar"
  :main           ^{:skip-aot true} me.kronos.ses.api.core)
