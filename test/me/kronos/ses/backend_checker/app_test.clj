(ns me.kronos.ses.backend-checker.app-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.backend-checker.core-test :as t]
            [me.kronos.ses.backend-checker.app :as app]
            [clj-time.core :as time]
            [me.kronos.ses.redis :as redis]
            [cheshire.core :as json]
            [me.kronos.ses.init]
            [clj-time.format :as time-format]
            [me.kronos.ses.queue :as queue]
            [clojure.core.async :as a :refer [>!! <!!]]
            [me.kronos.ses.constants :as constants]
            [me.kronos.ses.mail :as mail]))

(use-fixtures :each t/init-system)

(deftest backend-key-test
  (testing "correctly makes key"
    (is (= "ses:email-backend-:a"
           (#'app/backend-key :a)))))

(deftest next-stage-test
  (testing "returns first state when is called without args"
    (with-redefs [time/now (fn [] "now")]
      (is (= {:stage 0 :failed_at "now"} (#'app/next-stage)))))
  (testing "increments state"
    (with-redefs [time/now (fn [] "now")]
      (is (= {:stage 6 :failed_at "now"} (#'app/next-stage 5))))))

(deftest available-backends-test
  (let [<app>   (:app @t/system)
        <redis> (:redis @t/system)]
    (testing "returns empty array when key is not present"
      (is (= [] (#'app/available-backends <app>))))
    (testing "returns keywordized array"
      (redis/cache-json <redis> "ses:alive-email-backends" ["a" "b"] 100)
      (is (= [:a :b] (#'app/available-backends <app>))))))

(deftest backend-state-test
  (let [<app>   (:app @t/system)
        <redis> (:redis @t/system)]

    (testing "returns nil when there is no state in redis"
      (is (nil? (#'app/backend-state <app> :backend))))

    (testing "returns state"
      (let [time   "2018-07-08T11:48:46.815Z"
            state   {:stage 1 :failed_at "2018-07-08T11:48:46.815Z"}
            backend :backend]
        (redis/cache-json <redis> (#'app/backend-key :backend) state 100)
        (is (= {:stage 1 :failed_at (time-format/parse time)}
               (#'app/backend-state <app> :backend)))))))

(deftest backend-up-test
  (let [<app>   (:app @t/system)
        <redis> (:redis @t/system)
        <queue> (:redis-pubsub @t/system)]

    (testing "adds backend to state"
      (#'app/backend-up <app> "backend")
      (is (= ["backend"] (redis/lookup-json <redis> "ses:alive-email-backends"))))

    (testing "publishes backend to queue"
      (let [ch (a/timeout 50)]
        (queue/subscribe <queue> constants/backend-up-event (fn [[msg _ data]]
                                                              (when (= msg "message")
                                                                (>!! ch data))))
        (#'app/backend-up <app> "backend")
        (is (= "backend" (<!! ch)))))))

(deftest backends-up-test
  (let [<app>   (:app @t/system)
        <redis> (:redis @t/system)
        <queue> (:redis-pubsub @t/system)]

    (testing "publishes to queue and add to state array of backends"
      (let [ch (a/timeout 50)]
        (queue/subscribe <queue> constants/backend-up-event (fn [[msg _ data]]
                                                              (when (= msg "message")
                                                                (>!! ch data))))
        (#'app/backend-up <app> "backend1")
        (#'app/backend-up <app> "backend2")
        (is (= "backend1" (<!! ch)))
        (is (= "backend2" (<!! ch)))
        (is (= (set ["backend1" "backend2"])
               (set (redis/lookup-json <redis> "ses:alive-email-backends"))))))))

(deftest backend-down-test
  (let [<app>   (:app @t/system)
        <redis> (:redis @t/system)]

    (testing "ignores backend which is not in state"
      (let [backend :backend
            time    "2018-07-08T11:48:46.815Z"
            state   {:stage 1 :failed_at time}]
        (redis/cache-json <redis> "ses:alive-email-backends" ["a" "b"] 100)
        (redis/cache-json <redis> (#'app/backend-key backend) state 100)
        (#'app/backend-down <app> backend)
        (is (= [:a :b] (#'app/available-backends <app>)))
        (is (= {:stage 1 :failed_at (time-format/parse time)}
               (#'app/backend-state <app> backend)))))

    (testing "removes backend from available backend state"
      (let [backend :backend2]
        (redis/cache-json <redis> "ses:alive-email-backends" [(name backend)] 100)
        (#'app/backend-down <app> backend)
        (is (= [] (#'app/available-backends <app>)))))

    (testing "set up new failed stage"
      (let [backend :backend3]
        (redis/cache-json <redis> "ses:alive-email-backends" [(name backend)] 100)
        (#'app/backend-down <app> backend)
        (is (= {:stage 0}
               (-> (#'app/backend-state <app> backend)
                   (dissoc :failed_at))))))
    (testing "increments failed stage"
      (let [backend :backend4
            time    "2018-07-08T11:48:46.815Z"
            state   {:stage 1 :failed_at time}]
        (redis/cache-json <redis> "ses:alive-email-backends" [(name backend)] 100)
        (redis/cache-json <redis> (#'app/backend-key backend) state 100)
        (#'app/backend-down <app> backend)
        (is (= {:stage 2}
               (-> (#'app/backend-state <app> backend)
                   (dissoc :failed_at))))))))

(deftest checks-backend-test
  (let [<app>   (:app @t/system)
        <redis> (:redis @t/system)]
    (testing "makes available backend which is not marked as failed any more"
      (redis/cache-json <redis> "ses:alive-email-backends" #{(first mail/available-backends)} 100)
      (#'app/checks-backend nil <app>)
      (is (= mail/available-backends (#'app/available-backends <app>))))

    (testing "makes available backend which failed long time ago"
      (let [time-1     (json/generate-string (time/minus (time/now) (time/minutes 31)))
            time-2     (json/generate-string (time/minus (time/now) (time/minutes 29)))
            time-str-1 (subs time-1 1 (dec (.length time-1)))
            time-str-2 (subs time-2 1 (dec (.length time-2)))
            state-1    {:stage 2 :failed_at time-str-1}
            state-2    {:stage 2 :failed_at time-str-2}]
        (redis/cache-json <redis> "ses:alive-email-backends" [] 100)
        (redis/cache-json <redis> (#'app/backend-key :mailgun) state-1 100)
        (redis/cache-json <redis> (#'app/backend-key :sendgrid) state-2 100)
        (#'app/checks-backend nil <app>)
        (is (= [:mailgun] (#'app/available-backends <app>)))))))
