(ns me.kronos.ses.backend-checker.core-test
  (:require [clojure.test :refer :all]
            [com.stuartsierra.component :as component]
            [environ.core :refer [env]]
            [me.kronos.ses.common :as common]
            [me.kronos.ses.mail :as mail]))

(def system (atom nil))

(defn ->system
  [env]
  (component/system-using
    (component/system-map
      :env          env
      :app          {:backends (set mail/available-backends)}
      :redis        (common/->redis)
      :redis-pubsub (common/->queue))
    {:redis         [:env]
     :redis-pubsub  [:env]
     :app           [:redis :redis-pubsub]}))

(defn start []
  (-> (->system env)
      (component/start)))

(defn start-system []
  (reset! system (start)))

(defn stop-system []
  (swap! system component/stop))

(defn init-system
  [test-fn]
  (start-system)
  (test-fn)
  (stop-system))