(ns me.kronos.ses.common
  (:require [clojure.test :refer :all]
            [me.kronos.ses.redis :as redis]
            [clojure.core.async :as a :refer [<! >!]]
            [slingshot.slingshot :refer [throw+]]
            [com.stuartsierra.component :as component]
            [me.kronos.ses.queue :as queue]))

(defrecord FakeRedis [storage]
  component/Lifecycle
  (start [this]
    (assoc this :queues (atom {})))
  (stop [{:keys [queues] :as this}]
    (doseq [ch @queues]
      (a/close! ch))
    (assoc this :queues nil))

  redis/RedisProto
  (lookup-json [this key]
    (get @(:storage this) key))
  (within-lock [_ _ f]
    (f))
  (within-lock [_ _ _ _ f]
    (f))
  (cache-json [this k v ttl]
    (swap! (:storage this) #(assoc % k v)))
  (cmd [this cmd args]
    (let [cmd* (->> (re-find #"(?!\$)(\w+)@" (str cmd))
                    last)]
      (case cmd*
        "smembers" (get @(:storage this) args)
        "sadd"     (let [key       (first args)
                         value     (second args)
                         new-value (-> (get @(:storage this) key) (conj value))]
                     (swap! (:storage this) #(assoc % key new-value)))
        "srem"     (let [key       (first args)
                         value     (name (second args))
                         new-value (->> (get @(:storage this) key) (remove #(= % value)))]
                      (swap! (:storage this) #(assoc % key new-value)))))))

(defrecord FakeQueue []
  component/Lifecycle
  (start [this]
    (assoc this :queues (atom {})))
  (stop [{:keys [queues] :as this}]
    (doseq [ch (vals @queues)]
      (a/close! ch))
    (assoc this :queues nil))

  queue/Queue
  (subscribe [{:keys [queues]} queue f]
    (let [ch (a/chan (a/sliding-buffer 2))]
      (a/go-loop []
        (when-let [msg (<! ch)]
          (f msg)
          (recur)))
      (swap! queues assoc queue ch)))

  (publish [{:keys [queues]} queue msg]
    (when-let [ch (get @queues queue)]
      (a/put! ch ["message" 1 msg]))))

(defrecord BrokenQueue []
  component/Lifecycle
  (start [this]
         (assoc this :queues (atom {})))
  (stop [{:keys [queues] :as this}]
        (doseq [ch (vals @queues)]
          (a/close! ch))
        (assoc this :queues nil))

  queue/Queue
  (subscribe [{:keys [queues]} queue f]
    (throw+ "error subscribe"))

  (publish [{:keys [queues]} queue msg]
    (throw+ "error publish")))

(defn ->redis []
  (->FakeRedis (atom {})))

(defn ->queue []
  (->FakeQueue))

(defn ->broken-queue []
  (->BrokenQueue))
