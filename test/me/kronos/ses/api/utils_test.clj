(ns me.kronos.ses.api.utils-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.utils :as utils]))

(deftest generate-token-test
  (testing "it generates 30-symbols token"
    (is (= 30 (count (utils/generate-token))))
    (is (re-matches #"\A[A-Za-z0-9]{30}\z" (utils/generate-token)))))
