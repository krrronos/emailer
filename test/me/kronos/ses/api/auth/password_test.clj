(ns me.kronos.ses.api.auth.password-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.core-test :as t]
            [crypto.password.bcrypt :as password]
            [me.kronos.ses.api.auth.password :as password-auth]
            [me.kronos.ses.api.auth :as auth]))

(use-fixtures :once t/init-system)
(use-fixtures :each t/with-transaction)

(deftest user-by-credentials-test
  (let [<auth>    (:auth @t/system)
        <db>      (:db @t/system)
        user-data {:email (t/next-email) :password "1234abcd"}
        user      (->> (update user-data :password password/encrypt)
                       (t/new-user <db>))]
    (testing "returns nil for non-existent user"
      (is (nil? (password-auth/user-by-credentials <auth> {:email (t/next-email) :password (t/next-email)}))))

    (testing "returns nil when password is incorrect"
      (is (nil? (password-auth/user-by-credentials <auth> (assoc user-data :password "incorrect password")))))

    (testing "returns user"
      (is (= user (password-auth/user-by-credentials <auth> user-data))))))
