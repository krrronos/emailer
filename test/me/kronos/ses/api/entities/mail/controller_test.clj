(ns me.kronos.ses.api.entities.mail.controller-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.core-test :as t]
            [slingshot.test]
            [me.kronos.ses.api.entities.mail.controller :as controller]
            [me.kronos.ses.api.server.response :as response]
            [me.kronos.ses.api.entities.mail :as mail]
            [me.kronos.ses.queue :as queue]
            [me.kronos.ses.common :as common]))

(use-fixtures :once t/init-system)

(deftest create-test
  (let [<mail>     (:mail @t/system)
        valid-data {:from    "mail@gmail.com"
                    :to      "mail@gmail.com"
                    :subject "subject"
                    :text    "text"}]
    (testing "validates data"
      (doseq [key (keys valid-data)]
        (is (thrown+? #(= % {:type :validation_error :message :email_data_bad_format})
                      (controller/create {:json-params (dissoc valid-data key)
                                          :component   <mail>}))))

      (is (thrown+? #(= % {:type :validation_error :message :email_data_bad_format})
                    (controller/create {:json-params (assoc valid-data :from "not-a-email")
                                        :component   <mail>})))

      (is (thrown+? #(= % {:type :validation_error :message :email_data_bad_format})
                    (controller/create {:json-params (assoc valid-data :to "not-a-email")
                                        :component   <mail>}))))

    (testing "returns 202 ACCEPTED in case of success"
      (with-redefs [mail/send (fn [_ mail] true)]
        (is (= response/accepted
               (controller/create {:json-params valid-data :component <mail>})))))

    (testing "returns 422 Unprocessable Entity in case of fail"
      (let [<mail*> (assoc <mail> :queue (common/->broken-queue))]
        (is (= response/unprocessable-entity
               (controller/create {:json-params valid-data :component <mail*>})))))

    (testing "sends task to queue"
      (let [queue (atom {})]
        (with-redefs [mail/send (fn [_ mail]
                                  (reset! queue mail))]
          (controller/create {:json-params valid-data :component <mail>})
          (is (= valid-data @queue)))))))
