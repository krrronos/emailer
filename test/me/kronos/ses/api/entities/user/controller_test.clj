(ns me.kronos.ses.api.entities.user.controller-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.db-helper :as db-helper]
            [me.kronos.ses.api.core-test :as t]
            [clj-time.core :as time]
            [slingshot.test]
            [me.kronos.ses.api.entities.user.db :as user-db]
            [crypto.password.bcrypt :as password]
            [me.kronos.ses.api.entities.user.controller :as controller]
            [me.kronos.ses.api.server.response :as response]))

(use-fixtures :once t/init-system)
(use-fixtures :each t/with-transaction)

(deftest create-test
  (let [<user> (:user @t/system)]
    (testing "validates input body"
      ; bad email
      (is (thrown+? #(= % {:type :validation_error :message :invalid_signup_data})
                    (controller/create {:json-params {:email "A" :password "123asdsdfsdf"}
                                        :component <user>})))
      ; bad password
      (doseq [password ["123asds" "123456789" "abcdefghi"]]
        (is (thrown+? #(= % {:type :validation_error :message :invalid_signup_data})
                      (controller/create {:json-params {:email "valid@email.com" :password password}
                                          :component <user>})))))

    (testing "returns created user"
      (let [email (t/next-email)]
        (is (= {:status 200 :body {:user {:email email}}}
               (controller/create {:json-params {:email email :password "123asdsdfsdf"}
                                   :component <user>})))))

    (testing "handles duplicated emails"
      (let [<db>  (:db @t/system)
            email (t/next-email)]
        (t/new-user <db> {:email email})
        (is (= (response/api-error :validation_error :email_is_taken)
               (controller/create {:json-params {:email email :password "123asdsdfsdf"}
                                   :component <user>})))))))
