(ns me.kronos.ses.api.entities.user.db-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.db-helper :as db-helper]
            [me.kronos.ses.api.core-test :as t]
            [clj-time.core :as time]
            [slingshot.test]
            [me.kronos.ses.api.entities.user.db :as user-db]
            [crypto.password.bcrypt :as password]))

(use-fixtures :once t/init-system)
(use-fixtures :each t/with-transaction)

(deftest find-by-token-test
  (let [<db>  (:db @t/system)]
    (testing "returns nil for non-existent token"
      (is (nil? (user-db/find-by-token <db> (t/next-token)))))

    (testing "returns user"
      (let [user   (t/new-user <db>)
            token* (t/next-token)
            token  (db-helper/insert <db> :tokens {:token         token*
                                                   :refresh_token (t/next-token)
                                                   :user_id       (:id user)
                                                   :expired_at    (time/plus (time/now) (time/hours 2))})]
        (is (= (:email user)
               (-> (user-db/find-by-token <db> token*) :email)))))))

(deftest create-test
  (let [<db> (:db @t/system)]
    (testing "creates user"
      (let [user {:email (t/next-email) :password (password/encrypt (t/next-token))}]
        (is (= (:email user) (:email (user-db/create <db> user))))))))

(deftest find-by-email-test
  (let [<db> (:db @t/system)]
    (testing "returns nil for non-existent email"
      (is (nil? (user-db/find-by-email <db> (t/next-email)))))

    (testing "returns user"
      (let [user (t/new-user <db>)]
        (is (= user (user-db/find-by-email <db> (:email user))))))))


