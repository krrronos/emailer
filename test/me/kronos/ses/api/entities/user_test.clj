(ns me.kronos.ses.api.entities.user-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.entities.user :as user]))

(deftest ->public-user-test
  (testing "returns only email fields"
    (is (= {:email "email"}
           (user/->public-user {:id 1 :email "email" :password "bcrypted-password"})))))