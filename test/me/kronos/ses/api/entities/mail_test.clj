(ns me.kronos.ses.api.entities.mail-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.core-test :as t]
            [me.kronos.ses.api.entities.mail :as mail]
            [me.kronos.ses.queue :as queue]
            [clojure.core.async :as a :refer [<!! >!!]]
            [cheshire.core :as json]
            [me.kronos.ses.constants :as constants]))

(use-fixtures :once t/init-system)

(deftest send-test
  (let [<mail>   (:mail @t/system)
        <queue>  (:queue @t/system)]
    (testing "sends mail task to queue"
      (let [ch   (a/timeout 50)
            data {:a 1}]
        (queue/subscribe <queue> constants/queue (fn [[msg _ data]]
                                                   (when (= msg "message")
                                                     (->> (json/parse-string data true)
                                                          (>!! ch)))))
        (mail/send <mail> data)
        (is (= data (<!! ch)))))))
