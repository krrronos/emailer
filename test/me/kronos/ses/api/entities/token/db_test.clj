(ns me.kronos.ses.api.entities.token.db-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.db-helper :as db-helper]
            [me.kronos.ses.api.core-test :as t]
            [clj-time.core :as time]
            [me.kronos.ses.api.entities.token.db :as token-db]
            [slingshot.test]))

(use-fixtures :once t/init-system)
(use-fixtures :each t/with-transaction)

(deftest find-test
  (let [<db> (:db @t/system)]
    (testing "returns token"
      (let [token (t/next-token)
            data  {:token         token
                   :refresh_token (t/next-token)
                   :expired_at    (time/now)}]
        (token-db/create <db> data)
        (is (= data
               (-> (token-db/find <db> token)
                   (dissoc :user_id :refreshed_at))))))

    (testing "does not return deleted token"
      (let [token (t/next-token)
            data  {:token         token
                   :refresh_token (t/next-token)
                   :expired_at    (time/now)
                   :deleted_at    (time/now)}]
        (token-db/create <db> data)
        (is (nil? (token-db/find <db> token)))))

    (testing "does not return refreshed token"
      (let [token (t/next-token)
            data  {:token         token
                   :refresh_token (t/next-token)
                   :refreshed_at  (time/now)
                   :expired_at    (time/now)}]
        (token-db/create <db> data)
        (is (nil? (token-db/find <db> token)))))))

(deftest find-with-refresh-token-test
  (let [<db> (:db @t/system)]
    (testing "returns token"
      (let [token         (t/next-token)
            refresh-token (t/next-token)
            data           {:token         token
                            :refresh_token refresh-token
                            :expired_at    (time/now)}]
        (token-db/create <db> data)
        (is (= data
               (-> (token-db/find-with-refresh-token <db> token refresh-token)
                   (dissoc :user_id :refreshed_at))))))

    (testing "does not return deleted token"
      (let [token         (t/next-token)
            refresh-token (t/next-token)
            data           {:token         token
                            :refresh_token refresh-token
                            :expired_at    (time/now)
                            :deleted_at    (time/now)}]
        (token-db/create <db> data)
        (is (nil? (token-db/find-with-refresh-token <db> token refresh-token)))))

    (testing "does not return refreshed token"
      (let [token         (t/next-token)
            refresh-token (t/next-token)
            data           {:token         token
                            :refresh_token refresh-token
                            :expired_at    (time/now)
                            :refreshed_at  (time/now)}]
        (token-db/create <db> data)
        (is (nil? (token-db/find-with-refresh-token <db> token refresh-token)))))))

(deftest update-one-test
  (let [<db> (:db @t/system)]
    (testing "updates token"
      (let [id                    (t/next-token)
            another-refresh-token (t/next-token)
            token                 (token-db/create <db> {:token         id
                                                         :refresh_token (t/next-token)
                                                         :expired_at    (time/now)})
            updated-token         (token-db/update-one <db> id {:refresh_token another-refresh-token})]
        (is (not= another-refresh-token (:refresh_token token)))
        (is (= another-refresh-token (:refresh_token updated-token)))))))

(deftest create-test
  (let [<db> (:db @t/system)]
    (testing "creates token"
      (let [data {:token         "token"
                  :refresh_token "refresh_token"
                  :expired_at    (time/now)}]
        (is (= data
               (-> (token-db/create <db> data)
                   (dissoc :user_id :refreshed_at))))))))

(deftest delete-test
  (let [<db> (:db @t/system)]
    (testing "deletes token"
      (let [token (t/next-token)]
        (token-db/create <db> {:token         token
                               :refresh_token (t/next-token)
                               :expired_at    (time/now)})
        (token-db/delete <db> token)
        (is (nil? (token-db/find <db> token)))
        (is (some? (:deleted_at (db-helper/find-one <db> :tokens [:deleted_at] [:= :token token]))))))))
