(ns me.kronos.ses.api.entities.token.controller-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.core-test :as t]
            [me.kronos.ses.api.entities.token.controller :as controller]
            [slingshot.test]
            [me.kronos.ses.api.db-helper :as db-helper]
            [me.kronos.ses.api.server.response :as response]
            [crypto.password.bcrypt :as password]
            [me.kronos.ses.api.entities.token.db :as token-db]
            [me.kronos.ses.api.entities.token :as token]))

(use-fixtures :once t/init-system)
(use-fixtures :each t/with-transaction)

(deftest refresh-test
  (let [<token> (:token @t/system)
        <db>     (:db @t/system)]
    (testing "checks token"
      (let [user  (t/new-user <db>)
            token (token/create <token> {:user_id (:id user)})]
        (is (thrown+? #(= % {:type :validation_error :message :refresh_token_data_bad_format})
                      (controller/refresh {:json-params  {:refresh_token (:refresh_token token)}
                                                 :path-params {}
                                                 :component    <token>})))

        (is (thrown+? #(= % {:type :validation_error :message :refresh_token_data_bad_format})
                      (controller/refresh {:json-params  {:refresh_token (:refresh_token token)}
                                                 :path-params {:token " "}
                                                 :component    <token>})))))

    (testing "checks refresh_token"
      (let [user  (t/new-user <db>)
            token (token/create <token> {:user_id (:id user)})]
        (is (thrown+? #(= % {:type :validation_error :message :refresh_token_data_bad_format})
                      (controller/refresh {:json-params  {}
                                           :path-params {:token (:token token)}
                                           :component    <token>})))

        (is (thrown+? #(= % {:type :validation_error :message :refresh_token_data_bad_format})
                      (controller/refresh {:json-params  {:refresh_token " "}
                                           :path-params {:token (:token token)}
                                           :component    <token>})))))
    (testing "returns 404 for already used tokens"
      (let [user  (t/new-user <db>)
            token (token/create <token> {:user_id (:id user)})]
        (token/refresh <token> (:token token) {:refresh_token (:refresh_token token)})
        (is (= {:status 404}
               (controller/refresh {:json-params {:refresh_token (:refresh_token token)}
                                    :path-params {:token (:token token)}
                                    :component   <token>})))))

    (testing "returns 404 for nonexistent tokens"
      (let [user  (t/new-user <db>)
            token (token/create <token> {:user_id (:id user)})]
        (is (= {:status 404}
               (controller/refresh {:json-params {:refresh_token (:refresh_token token)}
                                    :path-params {:token "netoken"}
                                    :component   <token>})))))

    (testing "returns new token"
      (let [user      (t/new-user <db>)
            token     (token/create <token> {:user_id (:id user)})
            answer    (controller/refresh {:json-params  {:refresh_token (:refresh_token token)}
                                           :path-params {:token (:token token)}
                                           :component    <token>})
            new-token (db-helper/find-one <db> :tokens token-db/token-fields [:and
                                                                              [:= :user_id (:id user)]
                                                                              [:= :refreshed_at nil]])]
        (is (= {:status 200 :body {:token (token/->public-token new-token)}}
               answer))))))

(deftest create-test
  (let [<token> (:token @t/system)
        <db>    (:db @t/system)]
    ; bad email
    (is (thrown+? #(= % {:type :validation_error :message :auth_bad_format})
                  (controller/create {:json-params {:email "A" :password "123asdsdfsdf"}
                                      :component <token>})))
    ; bad password
    (doseq [password ["123asds" "123456789" "abcdefghi"]]
      (is (thrown+? #(= % {:type :validation_error :message :auth_bad_format})
                    (controller/create {:json-params {:email "valid@email.com" :password password}
                                        :component <token>}))))

    (testing "returns wrong_email_or_password if user does not exist or password is incorrect"
      (let [user (t/new-user <db> {:email (t/next-email) :password (password/encrypt "1234abcd")})]
        (is (= (response/api-error :invalid_operation :wrong_email_or_password)
               (controller/create {:json-params {:email (t/next-email) :password "1234abcde"} :component <token>})))))

    (testing "creates token and returns it"
      (let [user-data {:email (t/next-email) :password "1234abcd"}
            user      (->> (update user-data :password password/encrypt)
                           (t/new-user <db>))]
        (let [answer (controller/create {:json-params user-data :component <token>})
                tokens (db-helper/find <db> :tokens token-db/token-fields [:= :user_id (:id user)])]
          (is (= 1 (count tokens)))
          (is (= (-> answer :body :token)
                 (token/->public-token (first tokens)))))))))


(deftest delete-test
  (let [<token> (:token @t/system)
        <db>    (:db @t/system)]
    (testing "returns 404 if token not found"
      (is (= (response/not-found)
             (controller/delete {:path-params {:token (t/next-token)}
                                 :component <token>}))))
    (testing "deletes token"
      (let [token (:token (t/new-token <db>))]
        (is (= response/no-content
               (controller/delete {:path-params {:token token} :component <token>})))
        (is (nil? (token-db/find <db> token)))))))


;(defn refresh
;(defn delete
;  [{:keys [path-params] {<db> :db} :component}]
;  (if (token-db/delete <db> (:token path-params))
;    (response/no-content)
;    (response/not-found)))
