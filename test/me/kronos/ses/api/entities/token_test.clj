(ns me.kronos.ses.api.entities.token-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.core-test :as t]
            [me.kronos.ses.api.entities.token :as token]
            [clj-time.core :as time]
            [me.kronos.ses.api.db-helper :as db-helper]))

(use-fixtures :once t/init-system)
(use-fixtures :each t/with-transaction)

(deftest expired?-test
  (testing "checks expired_at against current time"
    (is (token/expired? {:expired_at (time/minus (time/now) (time/millis 1))}))
    (is (not (token/expired? {:expired_at (time/plus (time/now) (time/hours 2))}))))

  (testing "checks refreshed_at with nil"
    (let [future (time/plus (time/now) (time/hours 2))]
      (is (token/expired? {:expired_at future :refreshed_at (time/now)}))
      (is (not (token/expired? {:expired_at future :refreshed_at nil}))))))

(deftest create-test
  (let [<token> (:token @t/system)
        <db>    (:db @t/system)]
    (testing "creates token in database"
      (let [user   (t/new-user <db>)
            _      (token/create <token> {:user_id (:id user)})
            tokens (db-helper/find <db> :tokens [:token :refreshed_at :expired_at] [:= :user_id (:id user)])]
        (is (= 1 (count tokens)))
        (is (not (token/expired? (first tokens))))))))

(deftest refresh-test
  (let [<token> (:token @t/system)
        <db>     (:db @t/system)
        user     (t/new-user <db>)]

    (testing "returns new-token in case of success"
      (let [old-token (token/create <token> {:user_id (:id user)})
            new-token (token/refresh <token> (:token old-token) {:refresh_token (:refresh_token old-token)})]
        (is (not (= (:token old-token) (:token new-token))))
        (is (not (= (:refresh_token old-token) (:refresh_token new-token))))))

    (testing "updates old token's refreshed_at"
      (let [old-token (token/create <token> {:user_id (:id user)})
            new-token (token/refresh <token> (:token old-token) {:refresh_token (:refresh_token old-token)})]
        (is (not (nil? (db-helper/find-one <db> :tokens [:refreshed_at] [:= :token (:token old-token)]))))))

    (testing "returns nil in case of failure"
      (let [{:keys [token refresh_token]} (token/create <token> {:user_id (:id user)})]
        (is (nil? (token/refresh <token> (str token "a") {:refresh_token refresh_token})))
        (is (nil? (token/refresh <token> token {:refresh_token (str refresh_token "a")})))
        ; double refresh
        (is (not (nil? (token/refresh <token> token {:refresh_token refresh_token}))))
        (is (nil? (token/refresh <token> token {:refresh_token refresh_token})))))))

(deftest ->public-token-test
  (testing "returns only public fields"
    (let [now   (time/now)
          token {:token         "token"
                 :refresh_token "refresh_token"
                 :expired_at    now
                 :deleted_at    now}]

      (is (= {:token "token" :refresh_token "refresh_token" :expired_at now}
             (token/->public-token token))))))
