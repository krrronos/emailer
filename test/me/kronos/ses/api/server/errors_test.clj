(ns me.kronos.ses.api.server.errors-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.server.errors :as errors]))

(deftest api-errors-test
  (testing "wraps errors' array with errors key"
    (is (= {:errors [:error1 :error2]}
           (errors/api-errors [:error1 :error2]))))

  (testing "formats error"
    (is (= {:errors [{:type :type :message :message}]}
           (errors/api-errors :type :message)))))

(deftest error-test
  (testing "formats error")
  (is (= {:type "type" :message "message"}
         (errors/error "type" "message"))))
