(ns me.kronos.ses.api.server.interseptors-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.core-test :as t]
            [slingshot.test]
            [me.kronos.ses.api.server.interceptors :as interceptors]
            [me.kronos.ses.api.auth :as auth]
            [slingshot.slingshot :refer [throw+]]))

(use-fixtures :once t/init-system)
(use-fixtures :each t/with-transaction)

(defn call-interceptor
  ([interceptor]
   (call-interceptor interceptor {:request {}}))
  ([interceptor context]
   (let [f (:enter interceptor)]
     (f context))))

(deftest with-component-test
  (testing "assocs component to :component key"
    (let [component   "component"
          interceptor (interceptors/with-component component)]
      (is (= component
             (-> (call-interceptor interceptor)
                 :request :component))))))

(deftest require-user-test
  (testing "throws an exception"
    (is (thrown+? #(= % {:type :bad_auth :message :user_required})
                  (call-interceptor interceptors/require-user))))

  (testing "does nothing if current_user is presented in cotext"
    (let [context {:request {:current_user "user"}}]
      (is (= context (call-interceptor interceptors/require-user context))))))

(deftest auth-test
  (let [interceptor (interceptors/auth (:auth @t/system))]
    (testing "without authorization header returns context"
      (let [context {:request {}}]
        (is (= context (call-interceptor interceptor context)))))

    (testing "assocs current_user when token is valid"
      (let [user    "user"
            context {:request {:headers {"authorization" "Bearer 123"}}}]
        (with-redefs [auth/auth-user (fn [_ _] user)]
          (is (= user
                 (-> (call-interceptor interceptor context)
                     :request :current_user))))))

    (testing "returns error when token is invalid"
      (let [context {:request {:headers {"authorization" "Bearer 123"}}}]
        (with-redefs [auth/auth-user (fn [_ _] (throw+ {:type :me.kronos.ses.api.auth/bad-auth :message :bad_token}))]
          (is (= {:status 401 :body {:errors [{:type :bad_auth, :message :bad_token}]}}
                 (-> (call-interceptor interceptor context)
                     :response))))))))
