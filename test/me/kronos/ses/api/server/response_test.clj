(ns me.kronos.ses.api.server.response-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.server.response :as response]))

(deftest client-error-test
  (testing "returns body and 400 status by default"
    (is (= {:status 400 :body "some body"}
           (response/client-error "some body"))))

  (testing "returns body and status"
    (is (= {:status 422 :body "some body"}
           (response/client-error 422 "some body")))))

(deftest auth-error-test
  (testing "returns body with 401 error"
    (is (= {:status 401 :body "error"}
           (response/auth-error "error")))))

(deftest ok-test
  (testing "returns 200 and '{}' as a body by"
    (is (= {:status 200 :body {}}
           (response/ok))))

  (testing "returns 200 with custom body"
    (is (= {:status 200 :body {:a :b}}
           (response/ok {:a :b}))))

  (testing "returns custom status and body"
    (is (= {:status 201 :body "abc"}
           (response/ok 201 "abc"))))

  (testing "returns custom status, body and headers"
    (is (= {:status 201 :headers {:header1 "header1 value"} :body "abc"}
           (response/ok 201 {:header1 "header1 value"} "abc")))))

(deftest not-found-test
  (testing "returns 404 status"
    (is (= {:status 404}
           (response/not-found))))

  (testing "returns 404 with custom body"
    (is (= {:status 404 :body "custom body"}
           (response/not-found "custom body")))))

(deftest api-error-test
  (testing "formats one api error"
    (is (= {:status 400 :body {:errors [{:type :type, :message :message}]}}
           (response/api-error :type :message)))))
