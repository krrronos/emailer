(ns me.kronos.ses.api.db-helper
  (:require [clojure.test :refer :all]
            [honeysql.core :as sql]
            [honeysql.helpers :refer :all]
            [honeysql-postgres.format :refer :all]
            [honeysql-postgres.helpers :refer :all]
            [me.kronos.ses.api.db :as db])
  (:refer-clojure :exclude [delete update find partition-by]))

(defn insert
  [<db> table data]
  (-> (insert-into table)
      (values [data])
      (->> (partial returning))
      (apply (keys data))
      (sql/format)
      (->> (db/fetch-one <db>))))

(defn find-one
  [<db> table fields conds]
  (-> (apply select fields)
      (from table)
      (where conds)
      (sql/format)
      (->> (db/fetch-one <db>))))

(defn find
  ([<db> table fields]
   (-> (apply select fields)
       (from table)
       (sql/format)
       (->> (db/execute <db>))))
  ([<db> table fields conds]
   (-> (apply select fields)
       (from table)
       (where conds)
       (sql/format)
       (->> (db/execute <db>)))))

(defn delete
  [<db> table conds]
  (-> (update table)
      (sset {:deleted_at (sql/call :now)})
      (where conds)
      (sql/format)
      (->> (db/execute! <db>))))
