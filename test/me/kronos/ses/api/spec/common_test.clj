(ns me.kronos.ses.api.spec.common-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.spec.common :as spec-common]))

(deftest not-blank-test
  (testing "returns true if string is not blank"
    (is (spec-common/not-blank? "string"))
    (is (spec-common/not-blank? " string with spaces ")))

  (testing "returns false if string consists from only spaces or empty"
    (is (not (spec-common/not-blank? "")))
    (is (not (spec-common/not-blank? "   ")))))