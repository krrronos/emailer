(ns me.kronos.ses.api.auth-test
  (:require [clojure.test :refer :all]
            [me.kronos.ses.api.db-helper :as db-helper]
            [me.kronos.ses.api.core-test :as t]
            [clj-time.core :as time]
            [slingshot.test]
            [me.kronos.ses.api.auth :as auth]
            [me.kronos.ses.api.entities.user.db :as user-db]))

(use-fixtures :once t/init-system)
(use-fixtures :each t/with-transaction)

(deftest auth-user-test
  (let [<auth> (:auth @t/system)]
    (testing "returns expired-token when token has been expired"
      (let [<db>  (:db @t/system)
            user  (t/new-user <db>)
            token (db-helper/insert <db> :tokens {:token         "token"
                                                  :refresh_token "refresh_token"
                                                  :user_id       (:id user)
                                                  :expired_at    (time/now)})]
        (is (thrown+? #(= % {:type :bad_auth :message :expired_token})
                      (auth/auth-user <auth> "token")))))

    (testing "returns bad-token when token not found"
      (is (thrown+? #(= % {:type :bad_auth :message :bad_token})
                    (auth/auth-user <auth> "token-id-lol"))))

    (testing "returns bad-token when token is not passed"
      (is (thrown+? #(= % {:type :bad_auth :message :bad_token})
                    (auth/auth-user <auth> nil))))

    (testing "returns user-not-found for non existing user"
      (let [<db>  (:db @t/system)
            user  (t/new-user <db>)
            token (db-helper/insert <db> :tokens {:token         "token2"
                                                  :refresh_token "refresh_token2"
                                                  :user_id       (:id user)
                                                  :expired_at    (time/plus (time/now) (time/hours 2))})]
        (with-redefs [user-db/find-by-token (fn [_ _] nil)]
          (is (thrown+? #(= % {:type :bad_auth :message :user_not_found})
                        (auth/auth-user <auth> "token2"))))))

    (testing "returns valid user"
      (let [<db>  (:db @t/system)
            user  (t/new-user <db>)
            token (db-helper/insert <db> :tokens {:token         "token3"
                                                  :refresh_token "refresh_token3"
                                                  :user_id       (:id user)
                                                  :expired_at    (time/plus (time/now) (time/hours 2))})]
        (is (= (:email user)
               (:email (auth/auth-user <auth> "token3"))))))))
