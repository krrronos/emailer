(ns me.kronos.ses.api.core-test
  (:require [clojure.test :refer :all]
            [com.stuartsierra.component :as component]
            [me.kronos.ses.api.db.postgres :as postgres]
            [clojure.java.jdbc :as jdbc]
            [me.kronos.ses.api.entities.user.db :as user-db]
            [environ.core :refer [env]]
            [hikari-cp.core :as hikari]
            [instilled.cljdbc :as jdbc-wrapper]
            [me.kronos.ses.api.db :as db]
            [me.kronos.ses.api.db-helper :as db-helper]
            [instilled.cljdbc :as jdbc-wrapper]
            [crypto.password.bcrypt :as password]
            [clj-time.core :as time]
            [me.kronos.ses.api.entities.token.db :as token-db]
            [me.kronos.ses.redis :as redis]
            [clojure.core.async :as a :refer [<! >!]]
            [slingshot.slingshot :refer [throw+]]
            [me.kronos.ses.queue :as queue]
            [me.kronos.ses.common :as common]))

(def system (atom nil))
(def id (atom 1))

(declare ^:dynamic *txn*)

(defrecord PostgresWithRollback []
  component/Lifecycle
  (start [{:keys [env] :as this}]
    (let [datasource (-> (env :jdbc-database-url) (jdbc-wrapper/make-datasource {:hikari {}}))]
      (assoc this :datasource datasource :ds (:ds datasource))))
  (stop [this]
    (some-> (:ds this)
            (hikari/close-datasource))
    (assoc this :datasource nil))

  db/Database
  (execute [this sql]
    ;    (log/info sql)
    (jdbc/query *txn* sql))
  (execute! [this sql]
    ;    (log/info sql)
    (jdbc/execute! *txn* sql))
  (is-duplicate-ex? [_ ex]
    (postgres/is-duplicate-ex?* ex))
  (execute-in-transaction [this f]
    (f *txn*)))

(defn ->db []
  (->PostgresWithRollback))

(defn ->system
  [env]
  (component/system-using
    (component/system-map
      :env    env
      :db     (->db)
      :server {}
      :queue  (common/->queue)
      :redis  (common/->redis)
      :token  {}
      :user   {}
      :mail   {}
      :auth   {})
    {:queue  [:env]
     :db     [:env]
     :redis  [:env]
     :user   [:db]
     :auth   [:db]
     :token  [:db :auth :redis]
     :mail   [:db :queue]
     :server [:env :auth :token :user :mail]}))

(defn start []
  (-> (->system env)
      (component/start)))

(defn start-system []
  (reset! system (start)))

(defn stop-system []
  (swap! system component/stop))

(defn init-system
  [test-fn]
  (start-system)
  (test-fn)
  (stop-system))

(defn with-transaction [f]
  (jdbc/with-db-transaction
    [transaction {:datasource (-> @system :db :ds)}]
    (jdbc/db-set-rollback-only! transaction)
    (binding [*txn* transaction] (f))))

(defn next-email []
  (str (swap! id inc) "@gmail.com"))

(defn next-token []
  (str "token-" (swap! id inc)))

(defn new-user
  ([<db>]
   (new-user <db> {}))
  ([<db> fields]
   (let [email (or (:email fields) (next-email))]
     (db-helper/insert <db> :users (merge {:email email :password (password/encrypt "password")} fields))
     (db-helper/find-one <db> :users user-db/user-fields [:= :email email]))))

(defn new-token
  ([<db>]
   (new-token <db> {}))
  ([<db> fields]
   (let [token        (or (:token fields) (next-token))
         expired_at   (time/plus (time/now) (time/months 1))
         default-data {:token token :refresh_token (next-token) :expired_at expired_at}]
     (db-helper/insert <db> :tokens (merge default-data fields))
     (db-helper/find-one <db> :tokens token-db/token-fields [:= :token token]))))
