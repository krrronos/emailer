# SES: Simple Email Service

## Description of the problem and solution

### Problem

Provide REST API for simple email sending. The service should not send
emails by itself, instead it should use 3rd party APIs (mailgun, sendgrid, etc).
If any of email APIs is down the service should smoothly switched to another one.

### Solution

![Architecture](./doc/architecture.png)

**Solution focuses on backend part.**

#### API
1. In front there is an REST API which accepts requests in json format, performs authorization and validation.
1. Authorization/Authentication is done by tokens which could be received through [API](https://gitlab.com/krrronos/emailer/tree/master/doc).
1. Tokens and users are stored in postgres database
1. On email sending requests the API pushes a task to rabbitmq and answers with 202.
If queue is down then answer will be 422 which means user's request can't be processed right now.

#### Worker
1. There are [workers](https://gitlab.com/krrronos/emailer/tree/master/src/me/kronos/ses/worker) - simple daemons that are waiting for new jobs
by subscribing to rabbitmq.
Jobs are distributed by round robin among the workers (this is done through rabbitmq).
1. All tasks in queue are persistent and must be acked by workers after successful execution.
1. A worker has hardcoded list of available 3rd party emails services.
During first initialization it considers all email services as available.
During the work, worker could be notified that some email service up or down. 
1. In the beginning of each task worker selects an available service tries to send an email through it. 
In case of fail it removes the service from its available list, notifies other workers
and tries to use another backend.
1. The only state of worker it is a list of available email services workers are extremely
easy to scale. 

#### Backend checker

1. Backend checker acts like a cron (fires every 1 minute) and performs checks for failed email services.
See more [here](https://gitlab.com/krrronos/emailer/tree/master/src/me/kronos/ses/backend_checker) 

### Technical choices

1. Clojure language were chosen for two reasons: it is good choice for
async/multithreaded code (due to its immutability) and
it is most comfortable language for me right now. 
1. Sendgrid and mailgun were chosen by random.
1. Heroku was selected because it is free and easy to bootstrap. In real world I'd chose AWS:
I think it's not good idea to start new project with your own hardware, and I have no chance
to try GCE.
1. Postgres is my favorite database and I selected it. For this project
I don't think that database really matters.
1. Redis are good for storing shared state and pub/sub without durability.
1. Rabbitmq are free to use on heroku so I have to chose it. I have bad experience
with it and I'd chose kafka in real world app.

### Tradeoffs

1. I don't have time to cover worker with tests.
1. There is should be some exception aggregation. I'd chose sentry
1. Worker works in one thread and it is no good. Here should be a pool of
clojure's async goroutines.
1. Heroku [does not support](https://help.heroku.com/5BP0E8RC/how-to-enable-notify-keyspace-events-on-redis)
`notify-keyspace-events` so I don't consider it here. I'm not sure if it is a good choice
but at least I'd give it a try.  
1. Request body are not limited but it should be. 10-20 mb would be more than enough.
1. Rate-limiting at API level is a good feature for such type of services. I didn't have time to implement it.
1. Looks like mailgun and sendgrid libraries are not supports any types of timeout. The first
thing I'd change would be rewriting it.    
1. I haven't check redis' state about durability. If it is good and production ready
then rabbitmq could be dropped. 

### Monitoring

I failed to add it here but these thing I'd monitor in real world app:

1. Queue size
2. REST API response time
3. Availability of email services 

### Scaling

1. Workers could be scaled horizontally. Ideally it should be scaled (up and down)
automatically based on alers from queue size (growing - scale up, quiet - scale down).
2. Rest API could be scaled as well but there is should be a load balancer in front of it.
3. One redis instance should be enough. I don't have experience with redis cluster.
4. RabbitMQ cluster are pure evil and should be replaced by Kafka. 

### Documentation

API docs could be found [here](https://gitlab.com/krrronos/emailer/tree/master/doc)

### Deployed app

[Here](https://serene-crag-36774.herokuapp.com/)

### Experience

1. Clojure - 2,5 years
2. RabbitMQ - 1 week (I used to use it many years ago) 
3. Heroku - 1 week
4. Redis - 2 years

### Author

[Samsonov Ivan](https://www.linkedin.com/in/samsonovivan/)

### SLA

Nope, free heroku ¯\_(ツ)_/¯
