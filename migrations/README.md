# SES PostgreSQL migration

Repository contains required SQL migrations.

## 1. Requirements
* **Postgresql** database, version 9.1 or higher
* [**Flyway**](https://flywaydb.org/) database migration tool, version 5.0.2

## 2. Migration
Clone repo:
```bash
git clone https://gitlab.com/krrronos/emailer.git
```
cd to directory cloned/created:
```bash
cd emailer
```
Create database if it is not exists:
```bash
createdb ses
```
Run flyway command:
```bash
flyway -url=jdbc:postgresql://localhost:5432/ses -user=<login> -password=<password> -locations=filesystem:./migrations -baselineOnMigrate=true -baselineVersion=0 migrate
```
Ensure that you use correct credentials (`<login>` and `<password>`)
