CREATE TABLE users (
  id       SERIAL PRIMARY KEY,
  email    TEXT NOT NULL,
  password TEXT NOT NULL
);

CREATE UNIQUE INDEX users_email_unique_idx ON users (lower(email));

CREATE TABLE tokens (
  token         TEXT NOT NULL PRIMARY KEY,
  refresh_token TEXT NOT NULL,
  user_id       INT,
  refreshed_at  TIMESTAMP,
  expired_at    TIMESTAMP NOT NULL,
  deleted_at    TIMESTAMP
);

ALTER TABLE tokens ADD CONSTRAINT tokens_user_id_fk FOREIGN KEY (user_id) REFERENCES users (id);

CREATE UNIQUE INDEX tokens_refresh_token_unique_idx ON tokens (refresh_token);
