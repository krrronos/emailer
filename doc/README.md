# SES API

## Basic usage

1. Register a user
```bash
curl -v -X POST https://serene-crag-36774.herokuapp.com/users \
     -H "content-type: application/json" \
     -d '{"email": "hronya@gmail.com", "password": "password1"}'
```
2. Create token
```bash
curl -v -X POST https://serene-crag-36774.herokuapp.com/tokens \
     -H "content-type: application/json" \
     -d '{"email": "hronya@gmail.com", "password": "password1"}'
```
3. Send email
```bash
curl -v -X POST https://serene-crag-36774.herokuapp.com/mails \
     -H "Authorization: Bearer <token>" \
     -H "content-type: application/json" \
     -d '{"from": "from@mail.com", "to": "to@mail.com", "subject": "Subject", "text": "text", "from_name": "Ivan Ivanovich", "to_name": "Hronya hronevich"}'
```
HTTP response 202 means that everything is ok.

4. ????
5. Profit 

## API docs

### Authorization

API uses header base authorization with tokens. Example:
```
"Authorization: Bearer aengohpaduo8ew4Ohh0XoochuG9shi"
```

`POST /mails` is the only endpoint which requires authorization header. 

After creation token is valid for 1 month. 

### Errors

Error format is
```json
  {"errors": [{"type": "type", "message": "message"}]}
```

`type` indicates error's type, `message` indicates key of error message 
(errors are not translated). Common errors are described below.
   

| Type      | Message   | Where   | Why  |
|-----------|-----------|----------|-----|
|`bad_json` | `json_cant_be_parsed` | Any request with json body   | API can't parse provided json   |
|`bad_auth` | `bad_token`  | `POST /mails` request  | Token not found. Please request a new one |
|`bad_auth` | `expired_token`  | `POST /mails` request  |  Please, refresh token |
|`bad_auth` |  `user_not_found` | `POST /mails` request  | Definetly a server error. Request another token |

All other errors are described in [swagger](https://gitlab.com/krrronos/emailer/blob/master/swagger.yml). 

### API detailed descrption

See [swagger](https://gitlab.com/krrronos/emailer/blob/master/swagger.yml).

## How to run it locally

1. Install [leiningen](https://leiningen.org/)
2. Install [PostgreSQL](https://www.postgresql.org/) 9.1 or higher, 
3. Perform [migrations](https://gitlab.com/krrronos/emailer/tree/master/migrations)
4. Install [redis](https://redis.io/) at least 3.2.
5. Install [RabbitMQ](https://www.rabbitmq.com/) at least 3.7.6.
6. Run `cp .lein-env-example .lein-env` and edit all variables
7. Run `lein run`

## How to test

1. Install [leiningen](https://leiningen.org/)
2. Install PostgreSQL 9.1 or higher
3. Perform [migrations](https://gitlab.com/krrronos/emailer/tree/master/migrations)
4. Run `cp .lein-env-example .lein-env` and edit `jdbc-database-url`
5. Run `lein test`

## How to contribute

Please, contact me at `hronya@gmail.com`