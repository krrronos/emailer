(ns me.kronos.ses.backend-checker.app
  (:require [com.stuartsierra.component :as component]
            [clj-time.core :as time]
            [twarc.core :as twarc]
            [clojure.set :as set]
            [clojure.core.async :as a]
            [me.kronos.ses.redis :as redis]
            [me.kronos.ses.mail :as mail]
            [clojure.tools.logging :as log]
            [clj-time.format :as time-format]
            [me.kronos.ses.queue :as queue]
            [taoensso.carmine :as car]
            [me.kronos.ses.constants :as constants]
            [taoensso.carmine :as car]))

; Backend checker's work internal
(def ^:private check-interval
  (* 60 1000)) ; 1 minute

; Redis key for keeping backend checker's internal state
(def ^:private email-backends-key
  "ses:alive-email-backends")

; Backend checker scheduler name
(def scheduler-name
  "checks-backend-sched")

(def ^:private failing-stages
  ; joda-time -> milliseconds
  (map #(-> % .toPeriod .toStandardSeconds .getSeconds)
       [(time/minutes 1)
        (time/minutes 5)
        (time/minutes 30)
        (time/hours   1)
        (time/days    1)
        (time/weeks   1)
        (time/days    30)
        (time/days    356)
        (time/days    (* 50 365))
        (time/days    (* 50 365))])) ; indefinetly

(defn- backend-key
  [backend]
  (str "ses:email-backend-" backend))

(defn- next-stage
  ([]
   (next-stage -1))
  ([stage]
   {:stage (inc stage) :failed_at (time/now)}))

(defn- available-backends
  "Get available backends from internal state"
  [{:keys [redis]}]
  (or (some->> (redis/cmd redis car/smembers email-backends-key)
               (mapv keyword))
      []))

(defn- backend-state
  "Get failed backend's state from redis"
  [{:keys [redis]} backend]
  (some-> (backend-key backend)
          (->> (redis/lookup-json redis))
          (update :failed_at #(time-format/parse %))))

(defn- backend-up
  "Make backend available and notify workers about it"
  [{:keys [redis redis-pubsub]} backend]
  (redis/cmd redis car/sadd [email-backends-key backend])
  (queue/publish redis-pubsub constants/backend-up-event backend))

(defn- backends-up
  "Make list of backends available and notify workers about it"
  [<app> backends]
  (doseq [backend backends]
    (backend-up <app> backend)))

(defn- backend-down
  "Make backend unavailable"
  [{:keys [redis] :as <app>} backend]
  (let [key      (backend-key backend)
        ; avoid potential bug when multiple workers report a problem
        ; at the same time
        removed? (redis/within-lock redis (str key "-lock") 15000 10
                   (fn []
                     (when (-> (available-backends <app>)
                               (set)
                               (contains? backend))
                       (redis/cmd redis car/srem [email-backends-key backend])
                       true)))]
    (when removed?
      (if-let [{:keys [stage]} (redis/lookup-json redis key)]
        (redis/cache-json redis key (next-stage stage) (nth failing-stages (+ 2 stage)))
        (redis/cache-json redis key (next-stage) (second failing-stages))))))

(defn- checks-backend
  "Check if it is possible to bring some of backends back"
  [_ {:keys [redis backends] :as <app>}]
  (doseq [backend (set/difference backends (set (available-backends <app>)))]
    (let [{:keys [stage failed_at] :as state} (backend-state <app> backend)]
      (when (or (nil? state) (time/after?
                               (time/now)
                               (time/plus failed_at (time/seconds (nth failing-stages stage)))))
        (backend-up <app> backend)))))

(defrecord App [death-channel]
  component/Lifecycle
  (start [{:keys [redis redis-pubsub checks-backend-sched] :as this}]
    (queue/subscribe
      redis-pubsub
      constants/backend-down-event
      (fn [[msg _ backend]]
        (when (= msg "message")
          (log/info "backend down:" backend)
          (backend-down this backend))))

    ; if internal state is already in redis then load it
    ; otherwise initialize it
    (if (= 1 (redis/cmd redis car/exists email-backends-key))
      (let [backends (->> (redis/cmd redis car/smembers email-backends-key)
                          (mapv keyword))]
        (backends-up this backends)
        (log/info "Restore backend checker state with" backends))
      (do
        (backends-up this mail/available-backends)
        (log/info "Initialize backend checker state with" mail/available-backends)))

    ; if internal state is already in redis then load it
    ; otherwise initialize it
    (twarc/schedule-job checks-backend-sched
                        #'checks-backend [this]
                        :job     {:identity scheduler-name}
                        :trigger {:simple   {:repeat   :inf
                                             :interval check-interval}
                                  :start-at (.toDate (time/plus (time/now) (time/millis check-interval)))}
                        :replace true)
    (assoc this :backends (set mail/available-backends)))
  (stop [{:keys [checks-backend-sched] :as this}]
    (twarc/delete-job checks-backend-sched scheduler-name)
    (a/close! death-channel)
    (assoc this :backends nil)))

(defn create
  [death-channel]
  (->App death-channel))
