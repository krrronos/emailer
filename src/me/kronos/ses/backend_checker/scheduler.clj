(ns me.kronos.ses.backend-checker.scheduler)

; Default scheduler options for quartz
(def scheduler-properties
  {:threadPool.threadCount    2
   :plugin.triggHistory.class "org.quartz.plugins.history.LoggingTriggerHistoryPlugin"
   :plugin.jobHistory.class   "org.quartz.plugins.history.LoggingJobHistoryPlugin"
   :threadPool.class          "org.quartz.simpl.SimpleThreadPool"})
