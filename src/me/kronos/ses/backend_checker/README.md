# SES Backend checker

## Architecture

Backend checker is a cron like daemon which keeps track of failed/available email backends.
Whenever here is an `backend-down` event the checker removes it from available list (see below).
When backend could be considered as up checker adds it to backend available list and notifies 
worker about it.

### Failing stages

Failing stage is an array: index means stage number, value - amount of time during which
backend will be considered as down.
Failing stage in redis is a map of two keys: stage (stage's number) and failed_at - failing timestamp.
When backends fails for the first time backend checker creates a record in redis with zero
stage number and failed_at equals to now. Also checker adds a ttl to this record which
is equal to the next stage value.
If backends failed for the second time and there is an alive record in redis then stage and ttl
will be incremented. 
Example: on the first failing backend will be disabled for 1 minute and its ttl will be 5 minutes.
After 1 minute backend become available and if it failes in next 4 minutes it will be disabled
for 5 minutes (stage #1) and ttl will be equals to 30 minutes (stage #2).

### Backend down

1. Remove backend from list of available backends
2. Set proper failing stage (see above) in redis. 

### Backend up

Every 1 minute backend checker looks if there are some failed backends and if it is
possible to make some of them available.

Backend considered as up if one of two conditions is met:
1. There is no failed backend state in redis. It means that backend wasn't tried for
considerable amount of time. And it's possible that it is up now.
1. Last failing time was more than current stage's duration ago. In that case backend
is marked as available but its failing state remains in redis.   

## Scaling

There is always one running unit of backend checker and it is not supposed to be scaled.
If it is failed for some reason it could restore its state from redis.

## Redis state

| Key       | Data type   | Description   | Example  |
|-----------|------------|---------------|----------|
|`ses:alive-email-backends` | set | List of available backends   | `["mailgun", "sendgrid"]`   |
|`ses:email-backend-:<backend>` | string | Keeps data when and on which stage backend had failed | `{"stage": 0, "failed_at": "2015-08-09T18:31:42Z"}` |
