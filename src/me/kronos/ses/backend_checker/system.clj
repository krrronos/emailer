(ns me.kronos.ses.backend-checker.system
  (:require [com.stuartsierra.component :as component]
            [me.kronos.ses.backend-checker.app :as app]
            [me.kronos.ses.redis :as redis]
            [twarc.core :as twarc]
            [me.kronos.ses.backend-checker.scheduler :as scheduler]
            [me.kronos.ses.queue.redis :as redis-pubsub]))

(defn ->system
  [env death-channel]
  (component/system-using
    (component/system-map
      :env                  env
      :app                  (app/create death-channel)
      :redis                (redis/create)
      :checks-backend-sched (twarc/make-scheduler scheduler/scheduler-properties
                                                  {:name app/scheduler-name})
      :redis-pubsub         (redis-pubsub/create))
    {:redis                 [:env]
     :redis-pubsub          [:env]
     :app                   [:redis :redis-pubsub :checks-backend-sched]}))
