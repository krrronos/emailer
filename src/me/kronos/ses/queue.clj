(ns me.kronos.ses.queue)

(defprotocol Queue
  (subscribe [this queue f])
  (publish [this queue message]))
