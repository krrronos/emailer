(ns me.kronos.ses.worker.mail.sendgrid
  (:require [com.stuartsierra.component :as component]
            [me.kronos.ses.mail :as mail]
            [clojure.tools.logging :as log]
            [sendgrid.core :as sg]))

(defn- ->sendgrid
  [{:keys [text] :as mail}]
  (-> (select-keys mail [:to :from :subject])
      (assoc :message text)))

(defn- send*
  [{:keys [token]} mail]
  (try
    (let [answer (-> (->sendgrid mail)
                     (assoc :api-token token)
                     (sg/send-email))]
      (= 202 (:status answer)))
    (catch Exception e
      (log/error e)
      false)))

(defrecord Sendgrid []
  component/Lifecycle
  (start [{:keys [env] :as this}]
    (assoc this :name :sendgrid :token (str "Bearer " (env :sendgrid-token))))
  (stop [this]
    (assoc this :token nil :name nil))
  mail/Mail
  (send [this email]
    (send* this email)))

(defn create
  []
  (->Sendgrid))
