(ns me.kronos.ses.worker.mail.mailgun
  (:require [com.stuartsierra.component :as component]
            [clojure.tools.logging :as log]
            [mailgun.mail :as mailgun]
            [me.kronos.ses.mail :as mail]))

(defn- ->mailgun
  [{:keys [message] :as mail}]
  (select-keys mail [:to :from :subject :text]))

(defn- send*
  [{:keys [api-key domain]} mail]
  (try
    (let [answer (->> (->mailgun mail)
                      (mailgun/send-mail {:key api-key :domain domain}))]
      (= 200 (:status answer)))
    (catch Exception e
      (log/error e)
      false)))

(defrecord Mailgun []
  component/Lifecycle
  (start [{:keys [env] :as this}]
    (assoc this :api-key (env :mailgun-api-key) :domain (env :mailgun-domain) :name :mailgun))
  (stop [this]
    (assoc this :api-key nil :domain nil :name nil))
  mail/Mail
  (send [this email]
    (send* this email)))

(defn create
  []
  (->Mailgun))
