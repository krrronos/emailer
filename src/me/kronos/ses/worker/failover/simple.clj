(ns me.kronos.ses.worker.failover.simple
  (:require [com.stuartsierra.component :as component]
            [me.kronos.ses.queue :as queue]
            [me.kronos.ses.worker.failover :as failover]
            [clj-time.core :as time]
            [taoensso.carmine :as car]
            [clojure.set :as set]
            [clojure.tools.logging :as log]
            [twarc.core :as twarc]
            [clj-time.format :as time-format]
            [me.kronos.ses.mail :as mail]
            [me.kronos.ses.constants :as constants]))

(defrecord SimpleFailover []
  component/Lifecycle
  (start [{:keys [redis-pubsub] :as this}]
    (let [backends       (reduce #(assoc %1 %2 (%2 this)) {} mail/available-backends)
          alive-backends (atom (keys backends))
          this*          (assoc this :backends       backends
                                     :alive-backends alive-backends
                                     :counter        (atom 0))]

      (queue/subscribe
        redis-pubsub
        constants/backend-up-event
        (fn [[msg _ backend]]
          (when (= msg "message")
            (log/info "backend up:" backend)
            (swap! alive-backends conj (keyword backend)))))

      (queue/subscribe
        redis-pubsub
        constants/backend-down-event
        (fn [[msg _ backend]]
          (when (= msg "message")
            (log/info "backend down:" backend)
            (swap! alive-backends #(remove (fn [e] (= e (keyword backend))) %)))))

      this*))
  (stop [this]
    (assoc this :backends nil :alive-backends nil :counter nil))

  failover/Failover
  (select-backend [{:keys [backends alive-backends counter]}]
    (let [candidates @alive-backends]
      (when-not (empty? candidates)
        (->> (swap! counter #(-> % inc (mod (count candidates))))
             (nth candidates)
             (keyword)
             (get backends)))))
  (mark-backend-as-down [{:keys [redis]} backend]
    (queue/publish redis constants/backend-down-event backend)))

(defn create []
  (->SimpleFailover))
