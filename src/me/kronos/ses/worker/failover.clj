(ns me.kronos.ses.worker.failover)

(defprotocol Failover
  (select-backend [this])
  (mark-backend-as-down [this backend]))
