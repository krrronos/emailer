# SES Worker

## Architecture

Worker is a simple daemon which is connected to redis and rabbitmq.
It receives new jobs from rabbitmq and tries to execute it (see below). Also it
keeps track of available backends by subscribing to redis pubsub events.
Worker has internal state - list of available backends, on cold start
worker consider every of hardcoded backends are available.

## Initialization

1. Worker connects to redis & rabbitmq, subscribes to `emails` queue in rabbitmq and redis' 
`ses:backend-up` and `ses:backend-down` events. 
1. Worker has hardcoded available backends and consider that all of them are available.

## Handle emails

After each event from rabbitmq worker selects next backend among available and uses it to
send email. If there is no available backends an exception will be raised and email will stay in queue.
If during email sending happens an exception, the app marks current backend as bad, notify other workers and
try to send email with next backend. 

## Usage

1. Install [leiningen](https://leiningen.org/)
2. Install [PostgreSQL](https://www.postgresql.org/) 9.1 or higher, 
3. Perform [migrations](https://gitlab.com/krrronos/emailer/tree/master/migrations)
4. Install [redis](https://redis.io/) at least 3.2.
5. Install [RabbitMQ](https://www.rabbitmq.com/) at least 3.7.6.
6. Run `cp .lein-env-example .lein-env` and edit all variables
7. Run `lein run -m me.kronos.ses.worker.core`

