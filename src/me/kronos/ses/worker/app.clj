(ns me.kronos.ses.worker.app
  (:require [com.stuartsierra.component :as component]
            [clojure.core.async :as a]
            [me.kronos.ses.queue :as queue]
            [cheshire.core :as json]
            [me.kronos.ses.worker.failover :as failover]
            [me.kronos.ses.mail :as mail]
            [clojure.tools.logging :as log]
            [me.kronos.ses.constants :as constants]))

(defn message-handler
  "New mail handler. Every payloads is transformed
   to internal representation and is passed to one of
   mail backends. Mail backends are selected by round robin.
   On the exhaustion of mail backends an exception will be thrown"
  [{<failover> :failover} channel metadata ^bytes payload]
  (let [{:keys [from_name to_name user_id] :as mail} (-> (String. payload "UTF-8")
                                                         (json/parse-string true))
        mail* (cond-> mail
                      from_name (-> (dissoc :from_name)
                                    (update :from #(str from_name " <" % ">")))
                      to_name   (-> (dissoc :to_name)
                                    (update :to #(str to_name " <" % ">"))))]
    (log/info "new mail request")
    (loop [backend (failover/select-backend <failover>)]
      (if backend
        (when-not (mail/send backend mail*)
          (failover/mark-backend-as-down <failover> (:name backend))
          (recur (failover/select-backend <failover>)))
        (do
          (log/error "No available backends")
          (throw (Exception. "No available backends")))))))

(defrecord App [death-channel]
  component/Lifecycle
  (start [{:keys [queue] :as this}]
    (queue/subscribe queue constants/queue (partial message-handler this))
    this)
  (stop [this]
    (a/close! death-channel)
    this))

(defn create
  [death-channel]
  (->App death-channel))
