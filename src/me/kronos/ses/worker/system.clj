(ns me.kronos.ses.worker.system
  (:require [com.stuartsierra.component :as component]
            [me.kronos.ses.worker.app :as app]
            [me.kronos.ses.redis :as redis]
            [me.kronos.ses.queue.rabbitmq :as rabbitmq]
            [me.kronos.ses.queue.redis :as redis-pubsub]
            [me.kronos.ses.worker.mail.mailgun :as mailgun]
            [me.kronos.ses.worker.mail.sendgrid :as sendgrid]
            [me.kronos.ses.worker.failover.simple :as simple-failover]))

(defn ->system
  [env death-channel]
  (component/system-using
    (component/system-map
      :env          env
      :app          (app/create death-channel)
      :sendgrid     (sendgrid/create)
      :mailgun      (mailgun/create)
      :failover     (simple-failover/create)
      :redis        (redis/create)
      :queue        (rabbitmq/create)
      :redis-pubsub (redis-pubsub/create))
    {:sendgrid      [:env]
     :mailgun       [:env]
     :redis         [:env]
     :queue         [:env]
     :redis-pubsub  [:env]
     :failover      [:redis-pubsub :sendgrid :mailgun]
     :app           [:failover :queue]}))
