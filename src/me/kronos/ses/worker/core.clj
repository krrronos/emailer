(ns me.kronos.ses.worker.core
  (:require [clojure.tools.logging :as log]
            [me.kronos.ses.worker.system :as system]
            [com.stuartsierra.component :as component]
            [environ.core :refer [env]]
            [clojure.edn :as edn]
            [clojure.core.async :as a :refer [<!!]]
            [me.kronos.ses.init :as init]))

(defn -main [& args]
  (init/init-logging!)
  (let [death-channel (a/chan)
        sys (-> (system/->system env death-channel)
                (component/start))]
    (log/info "Worker has started")
    (.addShutdownHook (Runtime/getRuntime)
                      (Thread. #(component/stop sys)))
    (<!! death-channel)
    (log/info "Worker has stopped")))
