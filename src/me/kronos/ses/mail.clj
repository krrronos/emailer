(ns me.kronos.ses.mail
  (:refer-clojure :exclude [send]))

(defprotocol Mail
  (send [this mail]))

(def available-backends
  [:mailgun :sendgrid])