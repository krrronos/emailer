(ns me.kronos.ses.init
  (:require [clojure.tools.logging :as log])
  (:import (org.joda.time DateTime)
           (org.slf4j.bridge SLF4JBridgeHandler)))

(extend-protocol cheshire.generate/JSONable
  DateTime
  (to-json [dt gen]
    (cheshire.generate/write-string gen (str dt))))

(defn init-logging! []
  (SLF4JBridgeHandler/removeHandlersForRootLogger)
  (SLF4JBridgeHandler/install)
  (Thread/setDefaultUncaughtExceptionHandler
    (reify Thread$UncaughtExceptionHandler
      (uncaughtException [_ thread ex]
        (log/error ex "Uncaught exception on" (.getName thread))))))
