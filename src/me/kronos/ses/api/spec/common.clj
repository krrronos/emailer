(ns me.kronos.ses.api.spec.common
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as str]))

(defn not-blank?
  [string]
  (not (clojure.string/blank? string)))

(def email-regex #"\A[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\z")
(def token-regex #"\A[A-Za-z0-9]{30}\z")

(s/def ::email            (s/and string? #(re-matches email-regex %)))
; letters and numbers are required, at least 8 characters
(s/def ::password         (s/and string? #(re-matches #"\A(?=.*[A-Za-z])(?=.*\d).{8,}\z" %)))
(s/def ::not-empty-string (s/and string? not-blank?))

(s/def ::auth (s/keys :req-un [::email ::password]))
