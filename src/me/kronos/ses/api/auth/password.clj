(ns me.kronos.ses.api.auth.password
  (:require [crypto.password.bcrypt :as password]
            [me.kronos.ses.api.entities.user.db :as user-db]))

(defn user-by-credentials
  "Returns user by email and password. If email or password is invalid returns `nil`"
  [{<db> :db} {:keys [email password]}]
  (when-let [user (user-db/find-by-email <db> email)]
    (when (password/check password (:password user))
      user)))
