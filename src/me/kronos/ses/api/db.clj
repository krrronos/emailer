(ns me.kronos.ses.api.db
  (:require [clj-time.jdbc])) ; https://github.com/clj-time/clj-time/pull/176/files

(defprotocol Database
  (execute [this sql])
  (execute! [this sql])
  (execute-in-transaction [this f])
  (is-duplicate-ex? [this ex]))

(defn fetch-one
  [<db> sql]
  (-> (execute <db> sql)
      first))
