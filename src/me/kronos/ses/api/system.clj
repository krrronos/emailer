(ns me.kronos.ses.api.system
  (:require [com.stuartsierra.component :as component]
            [me.kronos.ses.api.server :as server]
            [me.kronos.ses.queue.rabbitmq :as rabbitmq]
            [me.kronos.ses.api.db.postgres :as postgres]
            [me.kronos.ses.redis :as redis]))

(defn ->system
  [env]
  (component/system-using
    (component/system-map
      :env    env
      :db     (postgres/create)
      :server (server/create)
      :queue  (rabbitmq/create)
      :redis  (redis/create)
      :token  {}
      :user   {}
      :mail   {}
      :auth   {})
    {:queue  [:env]
     :db     [:env]
     :redis  [:env]
     :user   [:db]
     :auth   [:db]
     :token  [:db :redis :auth]
     :mail   [:db :queue]
     :server [:env :auth :token :user :mail]}))

