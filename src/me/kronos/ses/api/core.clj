(ns me.kronos.ses.api.core
  (:require [cheshire.core]
            [clojure.tools.logging :as log]
            [me.kronos.ses.api.system :as system]
            [com.stuartsierra.component :as component]
            [environ.core :refer [env]]
            [me.kronos.ses.init :as init])
  (:gen-class))

(defn -main [& args]
  (init/init-logging!)
  (let [sys (-> (system/->system env)
                (component/start))]
    (.addShutdownHook (Runtime/getRuntime)
                      (Thread. #(component/stop sys)))))
