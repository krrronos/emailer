(ns me.kronos.ses.api.entities.user)

(def user-public-fields
  [:email])

(defn ->public-user
  "Coverts user to external format"
  [user]
  (select-keys user user-public-fields))