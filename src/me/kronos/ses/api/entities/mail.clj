(ns me.kronos.ses.api.entities.mail
  (:require [me.kronos.ses.queue :as queue]
            [cheshire.core :as json]
            [clojure.tools.logging :as log]
            [me.kronos.ses.constants :as constants])
  (:refer-clojure :exclude [send]))

(defn send
  "Converts mail to json and sends it to queue"
  [{<queue> :queue} mail]
  (try
    (->> (json/generate-string mail)
         (queue/publish <queue> constants/queue))
    true
    (catch Exception e
      (log/error e)
      false)))