(ns me.kronos.ses.api.entities.user.controller
  (:require [me.kronos.ses.api.server.response :as response]
            [slingshot.slingshot :refer [try+ throw+]]
            [me.kronos.ses.api.entities.user.spec :as spec]
            [me.kronos.ses.api.entities.user.db :as user-db]
            [me.kronos.ses.api.entities.user :as user]
            [crypto.password.bcrypt :as password]
            [me.kronos.ses.api.db :as db]))

(defn create
  "Creates new user"
  [{:keys [json-params] {<db> :db} :component}]
  (when-not (spec/valid-signup? json-params)
    (throw+ {:type :validation_error :message :invalid_signup_data}))
  (try+
    (->> (update json-params :password password/encrypt)
         (user-db/create <db>)
         (user/->public-user)
         (assoc {} :user)
         (response/ok))
    (catch (partial db/is-duplicate-ex? <db>) _
      (response/api-error :validation_error :email_is_taken))))
