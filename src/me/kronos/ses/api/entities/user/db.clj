(ns me.kronos.ses.api.entities.user.db
  (:require [me.kronos.ses.api.db :as db]
            [honeysql.helpers :as helpers]
            [honeysql-postgres.format :refer :all]
            [honeysql-postgres.helpers :refer :all]
            [honeysql.core :as sql]
            [me.kronos.ses.api.utils :as utils])
  (:refer-clojure :exclude [partition-by]))

(def user-fields
  [:id :email :password])

(def prefixed-user-fields
  (utils/wrap-with-prefix user-fields "users."))

(defn find-by-token
  "Returns user by token"
  [<db> token]
  (-> (apply helpers/select prefixed-user-fields)
      (helpers/from :users)
      (helpers/join :tokens [:= :tokens.user_id :users.id])
      (helpers/where [:and
                      [:= :tokens.token token]
                      [:> :tokens.expired_at (sql/call :now)]])
      (sql/format)
      (->> (db/fetch-one <db>))))

(defn create
  "Creates user"
  [<db> data]
  (-> (helpers/insert-into :users)
      (helpers/values [data])
      (->> (partial returning))
      (apply user-fields)
      (sql/format)
      (->> (db/fetch-one <db>))))

(defn find-by-email
  "Returns user by email"
  [<db> email]
  (-> (apply helpers/select prefixed-user-fields)
      (helpers/from :users)
      (helpers/where [:= :users.email email])
      (sql/format)
      (->> (db/fetch-one <db>))))
