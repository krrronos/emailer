(ns me.kronos.ses.api.entities.user.spec
  (:require [clojure.spec.alpha :as s]
            [me.kronos.ses.api.spec.common :as spec-common]))

(s/def ::user-signup :me.kronos.ses.api.spec.common/auth)

(defn valid-signup?
  [user-data]
  (s/valid? ::user-signup user-data))

