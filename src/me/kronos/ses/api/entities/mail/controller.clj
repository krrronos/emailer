(ns me.kronos.ses.api.entities.mail.controller
  (:require [me.kronos.ses.api.entities.mail.spec :as spec]
            [slingshot.slingshot :refer [throw+]]
            [me.kronos.ses.api.entities.mail :as mail]
            [me.kronos.ses.api.server.response :as response]))

(defn create
  "Accepts mail task from user, validates it and pushes to queue"
  [{:keys [json-params] <mail> :component}]
  (when-not (spec/valid-email-data? json-params)
    (throw+ {:type :validation_error :message :email_data_bad_format}))
  (if (mail/send <mail> json-params)
    response/accepted
    response/unprocessable-entity))