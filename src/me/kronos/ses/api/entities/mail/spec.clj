(ns me.kronos.ses.api.entities.mail.spec
  (:require [clojure.spec.alpha :as s]
            [me.kronos.ses.api.spec.common]))

(s/def ::from      :me.kronos.ses.api.spec.common/email)
(s/def ::to        :me.kronos.ses.api.spec.common/email)
(s/def ::subject   string?)
(s/def ::text      string?)
(s/def ::from_name :me.kronos.ses.api.spec.common/not-empty-string)
(s/def ::to_name   :me.kronos.ses.api.spec.common/not-empty-string)

(s/def ::email-data (s/keys :req-un [::from ::to ::subject ::text]
                            :opt-un [::from_name ::to_name]))

(defn valid-email-data?
  [email-data]
  (s/valid? ::email-data email-data))
