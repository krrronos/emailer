(ns me.kronos.ses.api.entities.token
  (:require [clj-time.core :as time]
            [me.kronos.ses.api.entities.token.db :as token-db]
            [me.kronos.ses.api.utils :as utils]
            [me.kronos.ses.redis :as redis]))

(def token-public-fields
  [:token :refresh_token :expired_at])

(defn expired?
  "Checks if provided token has been expired"
  [{:keys [expired_at refreshed_at]}]
  (or (not (nil? refreshed_at))
      (time/before? expired_at (time/now))))

(defn create
  "Creates new token"
  [{<db> :db} opts]
  (let [token-data (merge {:token         (utils/generate-token)
                           :refresh_token (str (java.util.UUID/randomUUID))
                           :expired_at    (time/plus (time/now) (time/months 1))}
                          opts)]
    (token-db/create <db> token-data)))

(defn refresh
  "Refresh token: create new and delete old"
  [{<db> :db <redis> :redis :as <token>} token {:keys [refresh_token]}]
  (let [lock-name (str token "-refresh")]
    (redis/within-lock <redis> lock-name (fn []
                                           (when-let [token* (token-db/find-with-refresh-token <db> token refresh_token)]
                                             (token-db/update-one <db> (:token token*) {:refreshed_at (time/now)})
                                             (create <token> {:user_id (:user_id token*)}))))))

(defn ->public-token
  "Coverts token to external format"
  [token]
  (select-keys token token-public-fields))