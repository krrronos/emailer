(ns me.kronos.ses.api.entities.token.db
  (:require [me.kronos.ses.api.db :as db]
            [honeysql.core :as sql]
            [honeysql.helpers :as helpers]
            [honeysql-postgres.format :refer :all]
            [honeysql-postgres.helpers :refer :all]
            [me.kronos.ses.api.utils :as db-utils]
            [clj-time.core :as time])
  (:refer-clojure :exclude [find partition-by]))

(def token-fields
  [:token :refresh_token :expired_at :user_id :refreshed_at])

(defn- find-one-with-conds
  [<db> conds]
  (-> (apply helpers/select token-fields)
      (helpers/from :tokens)
      (helpers/where conds)
      (sql/format)
      (->> (db/fetch-one <db>))))

(defn find
  "Finds token by its id"
  [<db> id]
  (find-one-with-conds <db> [:and
                             [:= :token id]
                             [:= :refreshed_at nil]
                             [:= :deleted_at nil]]))

(defn find-with-refresh-token
  "Finds token by its id and refresh-token"
  [<db> id refresh-token]
  (find-one-with-conds <db> [:and
                             [:= :token id]
                             [:= :refresh_token refresh-token]
                             [:= :deleted_at nil]
                             [:= :refreshed_at nil]]))

(defn update-one
  "Updates one token and returns result"
  [<db> id fields]
  (-> (helpers/update :tokens)
      (helpers/sset fields)
      (helpers/where [:and
                      [:= :deleted_at nil]
                      [:= :token id]])
      (->> (partial returning))
      (apply token-fields)
      (sql/format)
      (->> (db/fetch-one <db>))))

(defn create
  "Creates new token"
  [<db> data]
  (-> (helpers/insert-into :tokens)
      (helpers/values [data])
      (->> (partial returning))
      (apply token-fields)
      (sql/format)
      (->> (db/fetch-one <db>))))

(defn delete
  "Deletes token in database by updating deleted_at field"
  [<db> token-id]
  (update-one <db> token-id {:deleted_at (time/now)}))