(ns me.kronos.ses.api.entities.token.controller
  (:require [me.kronos.ses.api.entities.token.spec :as spec]
            [me.kronos.ses.api.entities.token :as token]
            [slingshot.slingshot :refer [throw+]]
            [me.kronos.ses.api.server.response :as response]
            [me.kronos.ses.api.entities.token.db :as token-db]
            [me.kronos.ses.api.auth.password :as password-auth]))

(defn refresh
  "Refreshes token with provided refresh token"
  [{:keys [json-params path-params] {<db> :db <redis> :redis :as <token>} :component}]
  (when-not (spec/valid-refresh-token-data? (merge path-params json-params))
    (throw+ {:type :validation_error :message :refresh_token_data_bad_format}))

  (if-let [user-id (some->> (:token path-params)
                            (token-db/find <db>)
                            :user_id)]
    (if-let [new-token (token/refresh <token> (:token path-params) json-params)]
      (->> (token/->public-token new-token)
           (assoc {} :token)
           (response/ok))
      (response/api-error :validation_error :token_cant_be_refreshed))
    (response/not-found)))

(defn create
  "Creates new token"
  [{:keys [json-params] {<db> :db <auth> :auth :as <token>} :component}]
  (when-not (spec/valid-auth? json-params)
    (throw+ {:type :validation_error :message :auth_bad_format}))
  (if-let [{:keys [id]} (password-auth/user-by-credentials <auth> json-params)]
    (->> (token/create <token> {:user_id id})
         (token/->public-token)
         (assoc {} :token)
         (response/ok))
    (response/api-error :invalid_operation :wrong_email_or_password)))

(defn delete
  "Deletes token"
  [{:keys [path-params] {<db> :db} :component}]
  (if (token-db/delete <db> (:token path-params))
    response/no-content
    (response/not-found)))
