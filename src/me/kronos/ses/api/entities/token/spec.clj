(ns me.kronos.ses.api.entities.token.spec
  (:require [clojure.spec.alpha :as s]
            [me.kronos.ses.api.spec.common :as spec-common]))

(s/def ::refresh_token :me.kronos.ses.api.spec.common/not-empty-string)
(s/def ::token         :me.kronos.ses.api.spec.common/not-empty-string)

(s/def ::refresh-token-data (s/keys :req-un [::refresh_token ::token]))

(defn valid-auth?
  [auth-data]
  (s/valid? :me.kronos.ses.api.spec.common/auth auth-data))

(defn valid-refresh-token-data?
  [refresh-token-data]
  (s/valid? ::refresh-token-data refresh-token-data))
