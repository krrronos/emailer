(ns me.kronos.ses.api.auth
  (:require [me.kronos.ses.api.entities.token :as token]
            [clojure.string :as str]
            [slingshot.slingshot :refer [throw+]]
            [me.kronos.ses.api.entities.token.db :as token-db]
            [me.kronos.ses.api.entities.user.db :as user-db]))

(defn ->token
  "Exctracts token from request"
  [token]
  (when (< 7 (.length token))
    (-> (subs token 7)
        str/trim)))

(defn auth-user
  "Authenticate user by token"
  [{<db> :db} token]
  (let [token*    (and token (token-db/find <db> token))
        user      (some->> (:token token*)
                           (user-db/find-by-token <db>))]
    (cond
      (nil? token*)           (throw+ {:type :bad_auth :message :bad_token})
      (token/expired? token*) (throw+ {:type :bad_auth :message :expired_token})
      (nil? user)             (throw+ {:type :bad_auth :message :user_not_found})
      :else user)))
