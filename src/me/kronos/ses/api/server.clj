(ns me.kronos.ses.api.server
  (:require [io.pedestal.http :as http]
            [io.pedestal.http.body-params :as body-params]
            [me.kronos.ses.api.server.response :as response]
            [me.kronos.ses.api.server.interceptors :as interceptors]
            [com.stuartsierra.component :as component]
            [plumbing.core :refer :all]
            [environ.core :refer [env]]
            [me.kronos.ses.api.entities.token.controller :as token-controller]
            [me.kronos.ses.api.entities.user.controller :as user-controller]
            [me.kronos.ses.api.entities.mail.controller :as mail-controller]
            [me.kronos.ses.api.spec.common :as spec-common]))

(defn interceptors
  [{<auth> :auth}]
  [http/json-body
   interceptors/error-handler
   (body-params/body-params)
   (interceptors/auth <auth>)])

(defn- ok
  [_]
  (response/ok {:result "ok"}))

(defn routes
  [{<token> :token
    <user>  :user
    <mail>  :mail
    :as <server>}]
  (let [common-interceptors (interceptors <server>)]
    {"/" {:interceptors            common-interceptors
          :get                     `ok
          "/users"                 {:interceptors [(interceptors/with-component <user>)]
                                    :post         `user-controller/create}
          "/tokens"                {:interceptors [(interceptors/with-component <token>)]
                                    :post         `token-controller/create}
          "/tokens/:token/refresh" {:interceptors [(interceptors/with-component <token>)]
                                    :put          `token-controller/refresh
                                    :constraints  {:token spec-common/token-regex}}
          "/mails"                 {:interceptors [interceptors/require-user
                                                   (interceptors/with-component <mail>)]
                                    :post         `mail-controller/create}}}))

(def default-config
  {::http/type            :jetty
   ::http/port            8080
   ::http/join?           false
   ::http/allowed-origins (fn [origin]
                            (when origin
                              (re-matches #"(?i)http://localhost(:\d+)?\z" origin)))})

(defrecord Server [service]
  component/Lifecycle
  (start [this]
    (if service
      this
      (cond-> default-config
        (env :port) (assoc ::http/port (Integer/parseInt (env :port)))
        true        (assoc ::http/routes  [[(routes this)]])
        true        http/create-server
        true        http/start
        true        ((partial assoc this :service)))))

  (stop [this]
    (when service
      (http/stop service))
    (assoc this :service nil)))

(defn create
  []
  (map->Server {}))
