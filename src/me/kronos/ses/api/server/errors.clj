(ns me.kronos.ses.api.server.errors)

(def bad-json
  {:type :bad_json :message :json_cant_be_parsed})

(defn api-errors
  ([type message]
   (api-errors [{:type type :message message}]))
  ([errors]
   {:errors errors}))

(defn error
  [type message]
  {:type type :message message})
