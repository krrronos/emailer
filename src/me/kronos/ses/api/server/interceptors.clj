(ns me.kronos.ses.api.server.interceptors
  (:require [io.pedestal.interceptor.error :as error]
            [me.kronos.ses.api.server.response :as response]
            [me.kronos.ses.api.server.errors :as errors]
            [clojure.tools.logging :as log]
            [slingshot.slingshot :refer [try+ throw+]]
            [me.kronos.ses.api.auth :as auth]
            [io.pedestal.interceptor.chain :as interceptor.chain])
  (:refer-clojure :exclude [error-handler]))
; http://pedestal.io/reference/error-handling
(def error-handler
  (error/error-dispatch [context ex]
    [{:exception-type :clojure.lang.ExceptionInfo}]
    (let [data (-> ex Throwable->map :data)]
      (case (some-> data :type)
        :validation_error
        (->> (errors/api-errors [data])
             (response/client-error)
             (assoc context :response))
        :invalid_operation
        (->> (errors/api-errors [data])
             (response/client-error)
             (assoc context :response))
        :bad_argument
        (->> (errors/api-errors [data])
             (response/client-error)
             (assoc context :response))
        :bad_auth
        (->> (errors/api-errors [data])
             (response/auth-error)
             (assoc context :response))
        (do
          (log/error ex)
          (assoc context :response {:status 500 :body "Internal server error"}))))
    [{:exception-type :com.fasterxml.jackson.core.JsonParseException}]
    (->> (errors/api-errors errors/bad-json)
         (response/client-error)
         (assoc context :response))
    [{:exception-type :com.fasterxml.jackson.core.io.JsonEOFException}]
    (->> (errors/api-errors errors/bad-json)
         (response/client-error)
         (assoc context :response))
    :else
    (do
      (log/error ex)
      (assoc context :response {:status 500 :body "Internal server error"}))))

(defn with-component
  "This interceptor helps to pass required component to controller"
  [<component>]
  {:name ::with-component
   :enter (fn [context]
            (assoc-in context [:request :component] <component>))})

(def require-user
  "This interceptor throws an error when someone is trying to get access to
   endpoints which is closed by authorization"
  {:name ::require-user
   :enter (fn [context]
            (when-not (-> context :request :current_user)
              (throw+ {:type :bad_auth :message :user_required}))
            context)})

(defn auth
  "This interceptor authenticate user and pass it to controller.
   In case of invalid auth data request will be terminated and error
   will be returned"
  [<auth>]
  {:name  ::auth
   :enter (fn [context]
            (if-let [token (get-in context [:request :headers "authorization"])]
              (try+
                (let [user (auth/auth-user <auth> (auth/->token token))]
                  (assoc-in context [:request :current_user] user))
                (catch [:type :me.kronos.ses.api.auth/bad-auth] {:keys [message]}
                  (interceptor.chain/terminate
                    (->> (errors/api-errors :bad_auth message)
                         (response/auth-error)
                         (assoc context :response)))))
              context))})

