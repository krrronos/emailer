(ns me.kronos.ses.api.server.response
  (:require [me.kronos.ses.api.server.errors :as errors]))

(defn- response
  "Base answer constructor"
  ([code]
   {:status code})
  ([code body]
   {:status code :body body})
  ([code headers body]
   {:status code :headers headers :body body}))

(defn client-error
  ([body]
   (client-error 400 body))
  ([code body]
   (response code body)))

(defn auth-error
  [body]
  (response 401 body))

(def no-content
  "No content responder"
  (response 204))

(def accepted
  "No content responder"
  (response 202))

(def unprocessable-entity
  (response 422))

(defn ok
  "Ok responder helper"
  ([]
   (ok 200 {}))
  ([answer]
   (ok 200 answer))
  ([code answer]
   (response code answer))
  ([code headers answer]
   (response code headers answer)))

(defn not-found
  "Ok responder helper"
  ([]
   (response 404))
  ([answer]
   (response 404 answer)))

(defn api-error
  [type message]
  (-> (errors/api-errors type message)
      (client-error)))
