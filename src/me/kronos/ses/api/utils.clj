(ns me.kronos.ses.api.utils
  (:require [clojure.set :as set]
            [clj-time.core :as time]
            [clojure.string :as str])
  (:import (org.joda.time Days)))

(defn- char-range [start end]
  (map char (range (int start) (inc (int end)))))

(def ^:private tokenable-symbols
  (concat (char-range \a \z) (char-range \A \Z) (char-range \0 \9)))

(defn- generate
  "Generates random string with length `n` and alphabet from `alphabet`"
  [alphabet length]
  (let [alphabet-size (count alphabet)
        next-int      #(.nextInt (java.security.SecureRandom.))
        chars         (->> #(nth alphabet (mod (next-int) alphabet-size))
                           (repeatedly)
                           (take length))]
    (apply str chars)))

(defn- format-column
  [prefix attr]
  (let [attr-str (name attr)]
    [(->> attr-str (str prefix) keyword) attr-str]))

(defn wrap-with-prefix
  "SQL helper. Add `prefix` prefix for every column from `array`
   Example: (wrap-with-prefix ['id', 'field'], 'orders.') =>
   [['orders.id', 'id'], ['orders.field', 'field']]"
  [array prefix]
  (map (partial format-column prefix) array))

(defn generate-token
  "Helper method for generating auth tokens"
  []
  (generate tokenable-symbols 30))
