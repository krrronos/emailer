(ns me.kronos.ses.api.db.postgres
  (:require [com.stuartsierra.component :as component]
            [hikari-cp.core :as hikari]
            [clojure.java.jdbc :as jdbc]
            [plumbing.core :refer :all]
            [clojure.tools.logging :as log]
            [me.kronos.ses.api.db :as db]
            [cheshire.core :as json]
            [honeysql.format :as fmt]
            [clojure.string :as str]
            [instilled.cljdbc :as jdbc-wrapper]))

; https://github.com/postgres/postgres/blob/master/src/backend/utils/errcodes.txt
(def ^:const unique-violation-error "23505")

(defn execute*
  [{:keys [conn] :as <db>} sql]
  (log/info sql)
  (if conn
    (jdbc/query conn sql)
    (jdbc/with-db-connection [conn <db>]
                             (jdbc/query conn sql))))

(defn execute!*
  [{:keys [conn] :as <db>} sql]
  (log/info sql)
  (if conn
    (jdbc/execute! conn sql)
    (jdbc/with-db-connection [conn <db>]
                             (jdbc/execute! conn sql))))

(defn is-duplicate-ex?*
  [ex]
  (and (= org.postgresql.util.PSQLException (type ex))
       (= unique-violation-error (.getSQLState ex))))

(defn execute-in-transaction*
  [<db> f]
  (jdbc/with-db-transaction [conn <db>]
                            (f (assoc <db> :conn conn))))

(defrecord Postgres []
  component/Lifecycle
  (start [{:keys [env] :as this}]
    (let [datasource (jdbc-wrapper/make-datasource (env :jdbc-database-url)
                       {:hikari {}})]
      (assoc this :datasource (:ds datasource))))
  (stop [this]
    (let [datasource (:datasource this)]
      (hikari/close-datasource datasource))
    (assoc this :datasource nil))

  db/Database
  (execute [this sql]
    (execute* this sql))

  (execute! [this sql]
    (execute!* this sql))

  (is-duplicate-ex? [_ ex]
    (is-duplicate-ex?* ex))

  (execute-in-transaction [this f]
    (execute-in-transaction* this f)))

(defn create []
  (->Postgres))
