(ns me.kronos.ses.redis
  (:require [com.stuartsierra.component :as component]
            [taoensso.carmine.locks :as locks]
            [taoensso.carmine :as car :refer (wcar)]
            [taoensso.carmine.connections :as conn]
            [clojure.tools.logging :as log]
            [cheshire.core :as json]))

(defprotocol RedisProto
  "Call function `f` with lock `lock-name`
   with optional max lock keeping time in ms `lock-time-ms` and
   lock wait time in ms `lock-wait-time-ms`"
  (within-lock [this lock-name f] [this lock-name lock-time-ms lock-wait-time-ms f])
  (cache-json [this key value ttl-sec])
  (lookup-json [this key])
  (cmd [this cmd args]))

(defn within-lock*
  ([this lock-name f]
   (within-lock* this lock-name 15000 500 f))
  ([{:keys [conn]} lock-name lock-time-ms lock-wait-time-ms f]
   (log/debug "trying to lock" lock-name)
   (locks/with-lock conn lock-name lock-time-ms lock-wait-time-ms
                    (log/debug "successfully locked" lock-name)
                    (let [res (f)]
                      (log/debug "releasing lock" lock-name)
                      res))))

(defn set*
  [{:keys [conn]} key val ttl-sec serialize-fn]
  (log/debug "Setting value for" key "with expiration time" ttl-sec)
  (let [val (serialize-fn val)]
    (car/wcar conn (car/set key val :ex ttl-sec))))

(defn lookup*
  [{:keys [conn]} key deserialize-fn]
  (log/debug "Looking in redis for" key)
  (->> (car/get key)
       (car/wcar conn)
       deserialize-fn))

(defn cmd*
  [{:keys [conn] :as <redis>} cmd args]
  (if (coll? args)
    (car/wcar conn (apply cmd args))
    (cmd* <redis> cmd [args])))

(defrecord Redis []
  component/Lifecycle
  (start [{:keys [env] :as this}]
    (try
      (let [conn {:pool (conn/conn-pool :mem/fresh {})
                  :spec {:uri (env :redis-url)}}]
        (car/wcar conn (car/ping))
        (assoc this :conn conn :listeners (atom [])))
      (catch Exception e
        (log/error e)
        this)))

  (stop [{:keys [listeners] :as this}]
    (when-let [conn-pool (get-in this [:conn :pool])]
      (.close conn-pool))
    (assoc this :conn nil))

  RedisProto
  (within-lock [redis lock-name f]
    (:result (within-lock* redis lock-name f)))
  (within-lock [redis lock-name f lock-time-ms lock-wait-time-ms]
    (:result (within-lock* redis lock-name f lock-time-ms lock-wait-time-ms)))
  (cache-json [redis key value ttl-sec]
    (set* redis key value ttl-sec json/encode))
  (lookup-json [redis key]
    (lookup* redis key #(json/decode % true)))

  (cmd [redis cmd args]
    (cmd* redis cmd args)))

(defn create []
  (->Redis))
