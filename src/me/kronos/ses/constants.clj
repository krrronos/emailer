(ns me.kronos.ses.constants)

(def backend-down-event
  "ses:backend-down")

(def backend-up-event
  "ses:backend-up")

; queue name for communicating between workers and API
(def queue "emails")
