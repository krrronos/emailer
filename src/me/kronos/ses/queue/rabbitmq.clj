(ns me.kronos.ses.queue.rabbitmq
  (:require [langohr.core :as rmq]
            [langohr.channel :as lch]
            [langohr.queue :as lq]
            [langohr.basic :as lb]
            [com.stuartsierra.component :as component]
            [environ.core :refer [env]]
            [me.kronos.ses.queue :as queue]
            [clojure.tools.logging :as log]
            [langohr.consumers :as lc]))

(def ^:private default-exchange-name "")
(def ^:private default-publish-options {:content-type "application/json" :persistent true})

(defn publish*
  [{:keys [ch]} queue message]
  (log/info "Publish new message")
  (lb/publish ch default-exchange-name queue message default-publish-options))

(defn subscribe*
  [{:keys [ch]} queue-name f]
  (lc/subscribe ch queue-name (lc/ack-unless-exception f) {:auto-ack false}))

(defrecord Rabbitmq []
  component/Lifecycle
  (start [this]
    (let [conn (rmq/connect {:uri (env :cloudamqp-url)})
          ch   (lch/open conn)]
      ; TODO: create queue outside code
      (lq/declare ch "emails" {:durable true :exclusive false :auto-delete false})
      (assoc this :ch ch :conn conn)))
  (stop [{:keys [conn ch] :as this}]
    (rmq/close ch)
    (rmq/close conn)
    (assoc this :ch nil :conn nil))

  queue/Queue
  (publish [this queue message]
    (publish* this queue message))
  (subscribe [this queue f]
    (subscribe* this queue f)))

(defn create
  []
  (->Rabbitmq))
