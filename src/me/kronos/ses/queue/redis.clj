(ns me.kronos.ses.queue.redis
  (:require [me.kronos.ses.queue :as queue]
            [com.stuartsierra.component :as component]
            [taoensso.carmine :as car :refer (wcar)]
            [taoensso.carmine.connections :as conn]
            [clojure.tools.logging :as log]))

(defn publish*
  [{:keys [conn]} queue message]
  (car/wcar conn (car/publish queue message)))

(defn subscribe*
  [{:keys [conn listeners]} queue f]
  (let [listener (car/with-new-pubsub-listener
                   (:spec conn)
                   {queue f}
                   (car/subscribe queue))]
    (swap! listeners conj listener)))

(defrecord RedisQueue []
  component/Lifecycle
  (start [{:keys [env] :as this}]
    (try
      (let [conn {:pool (conn/conn-pool :mem/fresh {})
                  :spec {:uri (env :redis-url)}}]
        (car/wcar conn (car/ping))
        (assoc this :conn conn :listeners (atom [])))
      (catch Exception e
        (log/error e)
        this)))

  (stop [{:keys [listeners] :as this}]
    (doseq [listener @listeners]
      (car/close-listener listener))
    (when-let [conn-pool (get-in this [:conn :pool])]
      (.close conn-pool))
    (assoc this :conn nil :listeners nil))

  queue/Queue
  (publish [this queue message]
    (publish* this queue message))
  (subscribe [this queue f]
    (subscribe* this queue f)))

(defn create
  []
  (->RedisQueue))
