openapi: 3.0.0
info:
  description: Simple email service API
  version: "1.0.0"
  title: SES REST API
  contact:
    email: hronya@gmail.com
tags:
  - name: user
    description: Everything related to users
  - name: token
    description: API's auth unit
  - name: mail
    description: Mail sending
paths:
  /users:
    post:
      tags:
        - user
      summary: User sign up
      requestBody:
        $ref: '#/components/requestBodies/UserSignUp'
      responses:
        '200':
          description: 'OK'
          content:
            application/json:
              schema:
                type: object
                properties:
                  user:
                    $ref: '#/components/schemas/PublicUser'
        '400':
          description: 'Email or password is invalid'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
              example:
                errors:
                  - type: "validation_error"
                    message: "invalid_signup_data|email_is_taken"
  /tokens:
    post:
      tags:
        - token
      summary: Creates new token
      requestBody:
        $ref: '#/components/requestBodies/NewTokenRequest'
      responses:
        '200':
          description: 'OK'
          content:
            application/json:
              schema:
                type: object
                properties:
                  token:
                    $ref: '#/components/schemas/Token'
        '400':
          description: 'Bad token data'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
              example:
                errors:
                  - type: "validation_error"
                    message: "auth_bad_format|wrong_email_or_password"
        '404':
          description: 'User with given phone number not found'
  /tokens/{token}:
    delete:
      tags:
        - token
      summary: Deletes token
      parameters:
        - in: path
          name: token
          schema:
            type: string
            pattern: '\A[A-Za-z0-9]{30}\z'
          required: true
          description: Token to be deleted
      responses:
        '204':
          description: 'Token has been deleted'
        '400':
          description: 'Token data is invalid'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
              example:
                errors:
                  - type: "validation_error"
                    message: "delete_token_data_bad_format"
        '404':
          description: 'Token not found'
  /tokens/{token}/refresh:
    put:
      tags:
        - token
      summary: Refreshes token
      parameters:
        - in: path
          name: token
          schema:
            type: string
          required: true
          description: Token to be refreshed
          example: "TOKEN123"
      requestBody:
        $ref: '#/components/requestBodies/RefreshTokenRequest'
      responses:
        '200':
          description: 'OK'
          content:
            application/json:
              schema:
                type: object
                properties:
                  token:
                    $ref: '#/components/schemas/Token'
        '400':
          description: 'Invalid token data'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
              example:
                errors:
                  - type: "validation_error"
                    message: "refresh_token_data_bad_format|token_cant_be_refreshed"
        '404':
          description: 'Token not found'
  /mails:
    post:
      tags:
        - mail
      summary: Sends mail
      requestBody:
        $ref: '#/components/requestBodies/SendMail'
      responses:
        '200':
          description: 'OK'
        '400':
          description: 'Bad email request'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
              example:
                errors:
                  - type: "validation_error"
                    message: "email_data_bad_format"
        '422':
          description: "The server can't handle user's request. Please, try later"
components:
  schemas:
    RefreshTokenRequest:
      type: object
      properties:
        refresh_token:
          type: string
          description: "Refresh token"
      required:
        - refresh_token
    Token:
      type: object
      properties:
        token:
          type: string
          description: "Token itself"
        refresh_token:
          type: string
          description: "Refresh token itself"
        expired_at:
          type: string
          description: "Token's expiration date"
          format: datetime
          example: "2012-04-23T18:25:43.511Z"
    NewTokenRequest:
      type: object
      properties:
        email:
          type: string
          format: email
          example: "email@gmail.com"
        password:
          type: string
          pattern: '\A(?=.*[A-Za-z])(?=.*\d).{8,}\z'
          example: "password123"
    UserSignUp:
      type: object
      properties:
        email:
          type: string
          format: email
          example: "email@gmail.com"
        password:
          type: string
          pattern: '\A(?=.*[A-Za-z])(?=.*\d).{8,}\z'
          example: "password123"
          description: "Password should be at least 8 chars and contain 1 letter and 1 digit"
    ErrorResponse:
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              type:
                type: string
              message:
                type: string
    PasswordAuth:
      type: object
      properties:
        email:
          type: string
          format: email
          example: "email@gmail.com"
        password:
          type: string
          example: "password"
    PublicUser:
      type: object
      properties:
        email:
          type: string
          format: email
          example: "email@gmail.com"
    SendMail:
      type: object
      properties:
        from:
          type: string
          format: email
          example: "from@gmail.com"
        to:
          type: string
          format: email
          example: "to@gmail.com"
        subject:
          type: string
          example: "Important message"
        text:
          type: string
          example: "Important message text"
        from_name:
          type: string
          example: "Samsonov Ivan"
        te_name:
          type: string
          example: "Alexey Pimenov"
      required:
        - from
        - to
        - subject
        - toxt
  requestBodies:
    UserSignUp:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/UserSignUp'
    NewTokenRequest:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/NewTokenRequest'
    RefreshTokenRequest:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/RefreshTokenRequest'
    PasswordAuth:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/PasswordAuth'
    SendMail:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/SendMail'